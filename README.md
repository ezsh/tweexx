# What is Tweexx?

Tweexx is a compiler/decompiler for Twine/Twee stories, implemented in C++. Tweex is very similar to [Tweego](http://www.motoslave.net/tweego/), was forked from it and should be generally similar in use.


## Why forking Tweego?

I wanted to make some changes to it but I don't like the Go language (Tweego is written in Go).

## Installation

You need a C++17 compatible compiler and CMake to build. The following packages are used:

Required:

 - Boost
 - spdlog

Optional:

 - Gumbo: for decompiling HTML stories
 - Iconv: for source encodings other than UTF-8
 - JsonCpp for parsing twine2 formats and story metadata. Fallback is boost::property_tree, which
   adds a bit to the library file size and is very strict about input formatting.
 - EFSW: to automatically recompile story when source files change

CMake configure log will tell what's missing and what options are available.

The basic commands to compile and install are:

```
mkdir build
cd build
cmake ..
cmake --build .
cmake --install .
```

## Usage

The compilation create 2 or three (if the tweexx option was enabled) executables: `twee2html` to compile stories, `html2twee` to decompile HTML stories, and `tweexx` which can serve as a drop-in replacement for the `tweego` executable (accepts the same options).

# Changes over Tweego

## Compiler

`twee2html` accept glob patterns for sources and modules (including '**').

## Decompiler

`html2twee` can extract and save embedded media files and can split the story file onto passages.




