/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "html2twee-config.h"

#include "toolsupport/toolsupport.hxx"
#include "twee/config.hxx"
#include "twee/decompiler.hxx"
#include "twee/exporter.hxx"
#include "twee/logging.hxx"
#include "twee/story.hxx"
#include "twee/types.hxx"
#include "twee/utils.hxx"

#include <dimcli/cli.h>

#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <utility>

namespace {
	enum class OutputFormat { Twee1, Twee3 };

	const std::filesystem::path stdoutSpecialPath = "-";

	struct Parameters {
		std::filesystem::path sourcefile; // slice of paths to seach for source files
		std::filesystem::path outPath = stdoutSpecialPath; // name of the output file
		struct {
			twee::string delimiterRegex;
			int maxDepth;
			unsigned minFragmentLength;
			unsigned minGroupSize;
			bool preserveSourceInfo; // do not override source file info from passage metadata
		} split;

		OutputFormat outputFormat = OutputFormat::Twee3; // output format
		bool extractMedia; // extract media passages into files and decode them from base64
		bool splitSources; // output passages as individual files
		bool logStats = false; // log story statistics
	};

	twee::TweeVersion outputFormatToTweeVersion(OutputFormat f)
	{
		switch (f) {
			case OutputFormat::Twee1:
				return twee::TweeVersion::Twee1;
			case OutputFormat::Twee3:
				return twee::TweeVersion::Twee3;
		}
		std::unreachable();
	}

	void dumpStoryToAFile(const twee::Story& story, const Parameters& params)
	{
		std::unique_ptr<std::ostream> outputFile;
		if (params.outPath != stdoutSpecialPath) {
			if (std::filesystem::is_directory(params.outPath)) {
				spdlog::critical("Output path may not point to a directory for the non-split mode");
				return;
			}
			outputFile = std::make_unique<std::ofstream>(params.outPath);

			if (outputFile->fail()) {
				spdlog::critical("Could not write to output file '{0}'", fmt::streamed(params.outPath));
			}
		}

		std::ostream& out = params.outPath == "-" ? std::cout : *outputFile;
		twee::storyToTwee(out, story, outputFormatToTweeVersion(params.outputFormat));
	}

	bool buildOutput(const Parameters& p)
	{
		// Create a new story instance and load the source files.
		const twee::Story s = twee::decompileHTML(p.sourcefile, "UTF-8");

		if (p.logStats) {
			tweexx::utils::printStoryStatistics(std::cout, s.statistics(), false, p.logStats);
		}

		// Write the output.
		if (!p.splitSources && !p.extractMedia) {
			dumpStoryToAFile(s, p);
			return true;
		}

		// now we output to set of files
		// outputPath has to point to a dir
		if (!std::filesystem::is_directory(p.outPath)) {
			spdlog::critical("Output path must point to an existing directory for the split mode");
			return false;
		}

		twee::SourceSplitOptions sso{p.extractMedia, p.splitSources};
		sso.delimiterRegex(p.split.delimiterRegex).maxDepth(p.split.maxDepth).preserveSourceInfo(p.split.preserveSourceInfo)
			.minFragmentLength(p.split.minFragmentLength).minGroupSize(p.split.minGroupSize);

		twee::storyToTwee(p.outPath, s, sso, outputFormatToTweeVersion(p.outputFormat));
		return true;
	}
} // namespace

int main(int argc, char** argv)
{
	Parameters params;
	const twee::SourceSplitOptions ssoDef;
	Dim::Cli cli;

	std::ios::sync_with_stdio(false);

	cli.desc("HTML stories de-compiler. Converts an HTML story file into a set of Twee text files and resources");
	std::ostringstream footer;
	footer << "Useful resources:\n"
		   << "\t- Twine interactive story builder: http://twinery.org/\n"
		   << "\t- Original Tweego compiler: https://www.motoslave.net/tweego/\n"
		   << "\t- Tweexx homepage: " << TWEEXX_HOMEPAGE_URL << '\n';
	cli.footer(footer.str());

	cli.opt(&params.sourcefile, "source s [source]").desc("Source file (HTML).").require();

	cli.group("output").title("Output").sortKey("2");
	cli.opt(&params.outputFormat, "twee-format", params.outputFormat)
		.desc("Output mode.")
		.choice(OutputFormat::Twee1, "1", "Twee1 format")
		.choice(OutputFormat::Twee3, "3", "Twee3 format");
	cli.opt(&params.outPath, "output o", params.outPath).desc("Output path or \"-\" to print to the standard output.");
	cli.opt(&params.extractMedia, "extract-media", params.extractMedia)
		.desc("Extract media passages (images, audio, video, etc.) and save as files in the output directory");
	cli.opt(&params.splitSources, "split", false)
		.desc("Split story source onto passages and save each passage in a file (see source split options below)");

	cli.group("split").title("Source split").sortKey("3");
	cli.opt(&params.split.delimiterRegex, "name-delim", ssoDef.delimiterRegex())
		.desc("Regular expression to split passage names and group them into folders");
	cli.opt(&params.split.minFragmentLength, "min-fragment-length", ssoDef.minFragmentLength())
		.desc("Minimal length for a passage name fragment. Shorter ones will be glued together");
	cli.opt(&params.split.minGroupSize, "min-group-size", ssoDef.minGroupSize())
		.desc("Minimal number of passages in a group.");
	cli.opt(&params.split.maxDepth, "max-depth", ssoDef.maxDepth())
		.desc("Maximal depth for nested passage folders");
	cli.opt(&params.split.preserveSourceInfo, "preserve-source-info", ssoDef.preserveSourceInfo())
		.desc("Preserve source file names found in the passage metadata");

	cli.group("log").title("Logging");
	cli.opt(&params.logStats, "log-stats l", params.logStats).desc("Log various story statistics.");
	tweexx::utils::LogOptions logOptions{cli};

	cli.versionOpt(std::string{twee::utils::versionString()});
	cli.helpNoArgs();

	if (!cli.parse(std::cerr, static_cast<unsigned>(argc), argv)) { return cli.exitCode(); }

	logOptions.apply();

	if (buildOutput(params)) { return 0; }
	return 1;
}
