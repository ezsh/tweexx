/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEX_TOOLSUPPORT_TOOLSUPPORT_HXX
#define TWEEX_TOOLSUPPORT_TOOLSUPPORT_HXX

#include <spdlog/common.h>
#include <spdlog/logger.h>

#include <filesystem>
#include <iosfwd>
#include <memory>

namespace Dim {
	class Cli;
}

namespace twee {
	struct StoryStatistics;
}

namespace tweexx::utils {
	/**
	 * @brief Ensures we can write to the directory
	 *
	 * @param p directory path to test.
	 * @param dirId id for the exception object
	 * @throws std::runtime_error
	 */
	void checkDirectoryExistsAndIsWritable(const std::filesystem::path& p, const std::string& dirId);

	class LogOptions {
	public:
		LogOptions(Dim::Cli& cli);
		void apply();
	private:
		spdlog::level::level_enum severity_;
		std::filesystem::path logFile_;
		std::shared_ptr<spdlog::logger> logger_;
	};

	void printStoryStatistics(std::ostream& os, const twee::StoryStatistics& statistics, bool printFiles, bool printContentStats);
}

#endif
