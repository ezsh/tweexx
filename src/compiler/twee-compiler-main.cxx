/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "twee2html-config.h"

#include "toolsupport/toolsupport.hxx"
#include "twee/compiler.hxx"
#include "twee/config.hxx"
#include "twee/formats.hxx"
#include "twee/logging.hxx"
#include "twee/story.hxx"
#include "twee/utils.hxx"

#include <dimcli/cli.h>

#include <algorithm>
#include <exception>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

namespace {
	enum class OutputMode { HTML, Twine1Archive, Twine2Archive };

	struct Parameters {
		std::vector<std::filesystem::path> sourcePaths; // slice of paths to search for source files
		std::vector<std::filesystem::path> excludePatters;
		std::vector<std::filesystem::path> modulePaths; // slice of paths to search for module files
		std::filesystem::path basePath;
		std::filesystem::path headFile; // name of the head file
		std::filesystem::path outFile{"-"}; // name of the output file
		std::string startingPassage;
		std::string formatID{"sugarcube-2"};
		std::string encoding{"UTF-8"};
		OutputMode outputMode{OutputMode::HTML}; // output mode
		bool testMode{false}; // enable test mode
		bool twee2Compat{false}; // enable Twee2 header extension compatibility mode
		bool trim{true}; // trim source files
		// 		bool watchFiles_; // enable filesystem watching
		bool logStats{false}; // log story statistics
		bool logFiles{false}; // log input files
	};

	bool buildOutput(const Parameters& p)
	{
		// Get the source and module paths.
		auto sourcePaths = twee::utils::collectFiles(p.sourcePaths, p.excludePatters); //, c.outFile)
		auto modulePaths = twee::utils::collectFiles(p.modulePaths, {});

		std::ranges::sort(sourcePaths);
		std::ranges::sort(modulePaths);

		// Create a new story instance and load the source files.
		twee::StoryLoadingOptions loadOptions{p.encoding, p.trim, p.twee2Compat, p.basePath};
		twee::Story s{sourcePaths, loadOptions, modulePaths};

		std::unique_ptr<std::ostream> outputFile;
		if (p.outFile != "-") { outputFile = std::make_unique<std::ofstream>(p.outFile); }

		std::ostream& out = p.outFile == "-" ? std::cout : *outputFile;
		// Write the output.
		twee::StoryCompilationOptions compileOpts{p.startingPassage, p.formatID, p.headFile};
		const twee::StoryFormatsMap formats{twee::environment::storyFormatDirs()};
		const twee::Compiler compiler{formats, compileOpts};

		switch (p.outputMode) {
			case OutputMode::Twine2Archive:
				// Write out the project as Twine 2 archived HTML.
				compiler.toTwine2Archive(out, s);
				break;
			case OutputMode::Twine1Archive:
				// Write out the project as Twine 1 archived HTML.
				compiler.toTwine1Archive(out, s);
				break;
			case OutputMode::HTML:
				// Basic sanity checks.
				if (!p.startingPassage.empty() && !s.contains(p.startingPassage)) {
					spdlog::critical("Starting passage \"{}\" not found.", p.startingPassage);
					return false;
				}

				compiler.toHTML(out, s);
				break;
		}

		if (p.logStats || p.logFiles) {
			tweexx::utils::printStoryStatistics(std::cout, s.statistics(), p.logFiles, p.logStats);
		}

		return true;
	}
} // namespace


int main(int argc, char** argv)
{
	Parameters params;
	bool listFormats;
	Dim::Cli cli;

	std::ios::sync_with_stdio(false);

	cli.desc("Twee to HTML compiler. Compiles Twine stories from story sources in Twee notation");
	std::ostringstream footer;
	footer << "Useful resources:\n"
		   << "\t- Twine interactive story builder: http://twinery.org/\n"
		   << "\t- Original Tweego compiler: https://www.motoslave.net/tweego/\n"
		   << "\t- Tweexx homepage: " << TWEEXX_HOMEPAGE_URL << std::endl;
	cli.footer(footer.str());

	cli.opt(&params.formatID, "format f", params.formatID).desc("ID of the story format");

	cli.group("source").title("Sources").sortKey("1");
	cli.optVec(&params.sourcePaths, "source [sources]")
		.desc("Source files or directories. Can be given multiple times and accepts glob patterns.");
	cli.optVec(&params.excludePatters, "exclude").desc("Glob-style exclude patters for the source files.");
	cli.opt(&params.encoding, "enconding e", params.encoding).desc("Encoding of input files.");
	cli.opt(&params.trim, "trim", params.trim).desc("Trim whitespaces around source passages.");
	cli.optVec(&params.modulePaths, "module m")
		.desc("Module sources (repeatable); may consist of supported "
			  "files and/or directories to recursively search for such files.");
	cli.opt(&params.twee2Compat, "twee2-compat.")
		.desc("Enable Twee2 source compatibility mode; files with "
			  "the .tw2 or .twee2 extensions automatically have compatibility mode enabled.");
	cli.opt(&params.headFile, "head")
		.desc("Name of the file whose contents will be appended "
			  "as-is to the <head> element of the compiled HTML.");
	cli.opt(&params.basePath, "base-dir", std::filesystem::current_path())
		.desc("Base directory for in-story component paths")
		.valueDesc("DIRPATH");

	cli.group("output").title("Output").sortKey("2");
	cli.opt(&params.outputMode, "output-mode", params.outputMode)
		.desc("Output mode.")
		.choice(OutputMode::HTML, "html", "Story HTML file")
		.choice(OutputMode::Twine1Archive, "twine1", "Twine 1 archive")
		.choice(OutputMode::Twine2Archive, "twine2", "Twine 2 archive");
	cli.opt(&params.outFile, "output o", params.outFile)
		.desc("Output file name or \"-\" to print to the standard output");
	cli.opt(&params.startingPassage, "start s")
		.desc("Name of the starting passage (default is the passage set by the story data, elsewise \"Start\".)");
	cli.opt(&params.testMode, "test. t", false)
		.desc("Compile in test mode; only for story formats in the Twine 2 style");
	cli.opt(&listFormats, "list-formats.", listFormats).desc("List the available story formats, then exit.");

	cli.group("log").title("Logging");
	cli.opt(&params.logFiles, "log-sources.", params.logFiles).desc("Log the processed input files.");
	cli.opt(&params.logStats, "log-stats. l", params.logStats).desc("Log various story statistics.");
	tweexx::utils::LogOptions logOptions{cli};

	cli.versionOpt(std::string{twee::utils::versionString()});
	cli.helpNoArgs();

	if (!cli.parse(std::cerr, static_cast<unsigned>(argc), argv)) { return cli.exitCode(); }

	logOptions.apply();

	try {
		if (listFormats) {
			twee::utils::printAvailableStoryFormats(std::cout);
			return 0;
		}
		if (buildOutput(params)) { return 0; }
		return 1;
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}
}
