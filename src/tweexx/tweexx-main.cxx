/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "tweexx-config.h"

#include "toolsupport/toolsupport.hxx"
#include "twee/compiler.hxx"
#include "twee/exporter.hxx"
#include "twee/formats.hxx"
#include "twee/logging.hxx"
#include "twee/story.hxx"
#include "twee/utils.hxx"

#include <dimcli/cli.h>

#include <algorithm>
#include <exception>

#if efsw_FOUND
#	include <efsw/efsw.hpp>
#endif

#if Gumbo_FOUND
#	include "twee/decompiler.hxx"
#endif

#include <fstream>
#include <iostream>
#include <set>

namespace {
	enum class OutputMode { HTML, Twee3, Twee1, Twine2Archive, Twine1Archive };

	struct Parameters {
		std::vector<std::filesystem::path> sourcePaths; // slice of paths to seach for source files
		std::vector<std::filesystem::path> excludePatters;
		std::vector<std::filesystem::path> modulePaths; // slice of paths to seach for module files
		std::filesystem::path headFile; // name of the head file
		std::filesystem::path outPath = "-"; // name of the output file
		OutputMode outputMode = OutputMode::HTML; // output mode
		std::string startingPassage;
		std::string formatID = "sugarcube-2";
		bool testMode = false; // enable test mode
		bool twee2Compat = false; // enable Twee2 header extension compatibility mode
		bool trim = true; // trim source files
		std::string encoding = "UTF-8";
		bool watchFiles = false; // enable filesystem watching
		bool logStats = false; // log story statistics
		bool logFiles = false; // log input files
	};

#if efsw_FOUND
	bool is_lexical_subpath(const std::filesystem::path& test, const std::filesystem::path& dir)
	{
		auto iTest = test.begin();
		for (auto iDir = dir.begin(); iDir != dir.end(); ++iDir) {
			if (iTest == test.end() || *iTest != *iDir) { return false; }
			++iTest;
		}
		return true;
	}

	struct WatchDirs {
		std::vector<std::filesystem::path> recursive;
		std::vector<std::filesystem::path> nonRecursive;
	};

	WatchDirs getWatchDirs(const Parameters& parameters)
	{
		const auto stripWildcards = [](const std::filesystem::path& p) {
			std::filesystem::path res;
			for (const auto& pathElem: p) {
				if (pathElem.string().find_first_of("*?") != std::string::npos) { break; }
				res /= pathElem;
			}
			return std::filesystem::is_directory(res) ? res : res.parent_path(); // because we can watch on dirs only
		};

		std::vector<std::filesystem::path> recursiveWatchPoints;
		std::set<std::filesystem::path> nonRecursiveWatchPointsSet;

		const auto addWatchPoint = [&](std::filesystem::path p) {
			p = std::filesystem::absolute(p);
			if (std::filesystem::exists(p) && !std::filesystem::is_directory(p)) {
				std::filesystem::path watchDir = p.parent_path();
				// this check is not strictly required, because we will do that again removing duplicates later,
				// but just to keep the list tidy...
				auto alreadyIncluded = std::any_of(
					recursiveWatchPoints.begin(), recursiveWatchPoints.end(),
					[&watchDir](const std::filesystem::path& recursivePath) {
						return is_lexical_subpath(watchDir, recursivePath);
					});
				if (!alreadyIncluded) { nonRecursiveWatchPointsSet.insert(watchDir); }
			} else {
				recursiveWatchPoints.push_back(stripWildcards(p));
			}
		};

		std::for_each(parameters.sourcePaths.begin(), parameters.sourcePaths.end(), addWatchPoint);
		std::for_each(parameters.modulePaths.begin(), parameters.modulePaths.end(), addWatchPoint);
		std::ranges::sort(recursiveWatchPoints);
		const auto newLast = std::unique(
			recursiveWatchPoints.begin(), recursiveWatchPoints.end(),
			[](const std::filesystem::path& left, const std::filesystem::path& right) {
				return is_lexical_subpath(right, left);
			});
		std::vector<std::filesystem::path> nonRecursiveWatchPoints;
		std::copy(nonRecursiveWatchPointsSet.begin(), nonRecursiveWatchPointsSet.end(),
			std::back_inserter(nonRecursiveWatchPoints));
		recursiveWatchPoints.erase(newLast, recursiveWatchPoints.end());
		auto iter = std::remove_if(
			nonRecursiveWatchPoints.begin(), nonRecursiveWatchPoints.end(), [&](const std::filesystem::path& p) {
				return std::any_of(
					recursiveWatchPoints.begin(), recursiveWatchPoints.end(),
					[&p](const std::filesystem::path& recursivePath) { return is_lexical_subpath(p, recursivePath); });
			});
		nonRecursiveWatchPoints.erase(iter, nonRecursiveWatchPoints.end());
		return {recursiveWatchPoints, nonRecursiveWatchPoints};
	}
#endif
} // namespace

bool buildOutput(const Parameters& p)
{
	// Get the source and module paths.
	auto sourcePaths = twee::utils::collectFiles(p.sourcePaths, p.excludePatters);
	auto modulePaths = twee::utils::collectFiles(p.modulePaths, {});

	std::ranges::sort(sourcePaths);
	std::ranges::sort(modulePaths);

	// Create a new story instance and load the source files.
	twee::StoryLoadingOptions loadOptions{p.encoding, p.trim, p.twee2Compat};
	bool areDecompiling = p.outputMode == OutputMode::Twee1 || p.outputMode == OutputMode::Twee3;
	twee::Story s = areDecompiling ?
#if Gumbo_FOUND
		twee::decompileHTML(sourcePaths[0], p.encoding)
#else
		Story("can't decompile HTML")
#endif
		: twee::Story{sourcePaths, loadOptions, modulePaths};

	std::unique_ptr<std::ostream> outputFile;
	if (p.outPath != "-") { outputFile.reset(new std::ofstream(p.outPath)); }

	std::ostream& out = p.outPath == "-" ? std::cout : *outputFile;
	// Write the output.
	twee::StoryCompilationOptions compileOpts{p.startingPassage, p.formatID, p.headFile};

	const twee::StoryFormatsMap formats{twee::environment::storyFormatDirs()};
	twee::Compiler compiler{formats, compileOpts};

	switch (p.outputMode) {
		case OutputMode::Twine2Archive:
			// Write out the project as Twine 2 archived HTML.
			compiler.toTwine2Archive(out, s);
			break;
		case OutputMode::Twine1Archive:
			// Write out the project as Twine 1 archived HTML.
			compiler.toTwine1Archive(out, s);
			break;
		case OutputMode::HTML:
			// Basic sanity checks.
			if (!p.startingPassage.empty() && !s.contains(p.startingPassage)) {
				spdlog::critical("Starting passage \"{}\" not found.", p.startingPassage);
				return false;
			}

			compiler.toHTML(out, s);
			break;
		case OutputMode::Twee1:
			twee::storyToTwee(out, s, twee::TweeVersion::Twee1);
			break;
		case OutputMode::Twee3:
			twee::storyToTwee(out, s, twee::TweeVersion::Twee3);
			break;
	}

	if (p.logStats || p.logFiles) {
		tweexx::utils::printStoryStatistics(std::cout, s.statistics(), p.logFiles, p.logStats);
	}

	return true;
}

#if efsw_FOUND
void watchLoop(const Parameters& parameters)
{
	class Listener: public efsw::FileWatchListener {
	public:
		Listener(const Parameters& p)
			: p_{p}
		{
		}

	private:
		void handleFileAction(
			efsw::WatchID /*watchid*/, const std::string& /*dir*/, const std::string& /*filename*/, efsw::Action /*d*/,
			std::string /*oldFilename*/) override
		{
			std::cerr << "File change detected, rebuilding output" << std::endl;
			buildOutput(p_);
		}
		const Parameters& p_;
	};

	WatchDirs wd = getWatchDirs(parameters);

	Listener listener{parameters};

	efsw::FileWatcher monitor;

	for (const auto& wp: wd.recursive) { monitor.addWatch(wp, &listener, true); }
	for (const auto& wp: wd.nonRecursive) { monitor.addWatch(wp, &listener, false); }

	std::cout << "Entering watch loop. Send end of file (^D or ^Z in Windows) to exit" << std::endl;
	char c;
	monitor.watch();
	while (std::cin.get(c)) {}
}
#endif

int main(int argc, char** argv)
{
	Parameters params;

	bool archiveTwine2 = false;
	bool archiveTwine1 = false;
	bool decompileTwee3 = false;
	bool decompileTwee1 = false;

	bool listCharsets = false;
	bool listFormats = false;

	Dim::Cli cli;

	cli.desc("HTML stories compiler/de-compiler. Converts between Twee text sources and HTML.");
	std::ostringstream footer;
	footer << "Useful resources:\n"
		   << "\t- Twine interactive story builder: http://twinery.org//n"
		   << "\t- Original Tweego compiler: https://www.motoslave.net/tweego/\n"
		   << "\t- Tweexx homepage: " << TWEEXX_HOMEPAGE_URL << std::endl;
	cli.footer(footer.str());

	cli.opt(&params.formatID, "format f", params.formatID).desc("ID of the story format");

	cli.group("output").title("Output").sortKey("0");
	cli.opt(&archiveTwine2, "archive-twine2. a").desc("Output Twine 2 archive, instead of compiled HTML.");
	cli.opt(&archiveTwine1, "archive-twine1.").desc("Output Twine 1 archive, instead of compiled HTML.");
#if Gumbo_FOUND
	cli.opt(&decompileTwee3, "decompile-twee3. d").desc("Output Twee 3 source code, instead of compiled HTML.");
	cli.opt(&decompileTwee1, "decompile-twee1.").desc("Output Twee 1 source code, instead of compiled HTML.");
#endif
	cli.opt(&params.outPath, "o output", params.outPath).desc("Name of the output file. '-' means the standard output");

	cli.group("source").title("Sources").sortKey("1");
	cli.optVec(&params.sourcePaths, "sources [sources]")
		.desc("Input sources (repeatable); may consist of supported files and/or directories to recursively search for "
			  "such files.");
	cli.opt(&params.encoding, "c charset", params.encoding).desc("Name of the input character set.");
	cli.opt(&params.formatID, "f format", params.formatID).desc("ID of the story format.");
	cli.opt(&params.headFile, "head", params.headFile)
		.desc("Name of the file whose contents will be appended as-is to the <head> element of the compiled HTML.");
	cli.optVec(&params.modulePaths, "m module")
		.desc("Module sources (repeatable); may consist of supported files and/or directories to recursively search "
			  "for such files.");

	cli.group("options").title("Options").sortKey("3");
	cli.opt(&listCharsets, "list-charsets.", listCharsets)
		.desc("List the supported input character sets, then exit. NOT IMPLEMENTED YET.");
	cli.opt(&listFormats, "list-formats.", listFormats).desc("List the available story formats, then exit.");
	cli.opt(&params.logFiles, "log-files.", params.logFiles).desc("Log the processed input files.");
	cli.opt(&params.logStats, "l log-stats.", params.logStats).desc("Log various story statistics.");

	cli.opt(&params.trim, "trim", params.trim).desc("Trim whitespace surrounding passages.");
	cli.opt(&params.startingPassage, "s start", params.startingPassage)
		.desc("Name of the starting passage.")
		.defaultDesc("the passage set by the story data");
	cli.opt(&params.testMode, "t test.", params.testMode)
		.desc("Compile in test mode; only for story formats in the Twine 2 style.");
	cli.opt(&params.twee2Compat, "twee2-compat.", params.twee2Compat)
		.desc("Enable Twee2 source compatibility mode; files with the .tw2 or .twee2 extensions automatically have "
			  "compatibility mode enabled.");
	cli.opt(&params.watchFiles, "w watch.", params.watchFiles)
		.desc("Start watch mode; watch input sources for changes, rebuilding the output as necessary.");
	tweexx::utils::LogOptions logOptions{cli};

	cli.versionOpt(std::string{twee::utils::versionString()});
	cli.helpNoArgs();

	if (!cli.parse(std::cerr, static_cast<unsigned>(argc), argv)) { return cli.exitCode(); }

	const int outputoptionsCount = static_cast<int>(archiveTwine1) + static_cast<int>(archiveTwine2) +
								   static_cast<int>(decompileTwee3) + static_cast<int>(decompileTwee1);
	if (outputoptionsCount > 1) {
		std::cerr << "Output type options are mutually exclusive" << std::endl;
		return 64; // EX_USAGE
	}

	if (archiveTwine1) {
		params.outputMode = OutputMode::Twine1Archive;
	} else if (archiveTwine2) {
		params.outputMode = OutputMode::Twine2Archive;
	} else if (decompileTwee1) {
		params.outputMode = OutputMode::Twee1;
	} else if (decompileTwee3) {
		params.outputMode = OutputMode::Twee3;
	} else {
		params.outputMode = OutputMode::HTML;
	}

	logOptions.apply();

	try {
		if (listFormats) {
			twee::utils::printAvailableStoryFormats(std::cout);
			return 0;
		}

		if (listCharsets) {
			std::cerr << "Charset listing is not implemented yet" << std::endl;
			return 1;
		}
#if efsw_FOUND
		if (params.watchFiles) {
			watchLoop(params);
		} else {
			return buildOutput(params) ? 0 : 1;
		}
#else
		return buildOutput(params) ? 0 : 1;
#endif
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}
}
