/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./config.hxx"

#include "./logging.hxx"
#include "./story.hxx"

#include "twee-config.h"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

#include <cstdlib>

namespace {
#ifdef WIN32
	const char homeDirEnvName[] = "USERPROFILE";
	const char pathListSeparator = ';';
#else
	const char homeDirEnvName[] = "HOME";
	const char pathListSeparator = ':';
#endif

	std::filesystem::path homeDirectory()
	{
		const char* envVar = std::getenv(homeDirEnvName);
		if (envVar) {
			return {envVar};
		}
		return {};
	}

	bool isDir(const std::filesystem::path& p)
	{
		return std::filesystem::is_directory(p) ||
		       (std::filesystem::is_symlink(p) && std::filesystem::is_directory(std::filesystem::read_symlink(p)));
	}

	std::vector<std::filesystem::path> splitDirList(const char* str)
	{
		std::vector<std::filesystem::path> res;
		boost::algorithm::split(res, str, [](auto ch){return ch == pathListSeparator;}, boost::token_compress_on);
		return res;
	}

	void appendExistingDirs(
		std::vector<std::filesystem::path>& res, const std::filesystem::path& baseDir,
		const std::vector<std::filesystem::path>& dirs)
	{
		if (!isDir(baseDir)) { return; }

		for (const auto& d: dirs) {
			const auto p = baseDir / d;
			if (isDir(p)) { res.push_back(p); }
		}
	}

	void appendExistingDirs(std::vector<std::filesystem::path>& res, const std::vector<std::filesystem::path>& dirs)
	{
		for (const auto& d: dirs) {
			const auto p = d;
			if (isDir(p)) { res.push_back(p); }
		}
	}

	void appendDirsFormEnvronmentVariable(std::vector<std::filesystem::path>& dirs, const char* envVarName)
	{
		if (const char* varValue = ::getenv(envVarName)) {
			appendExistingDirs(dirs, splitDirList(varValue));
		}
	}

#if 0
	const char* defaultFormatID = "sugarcube-2";
	const char* defaultOutFile = "-"; // <stdout>
	// 	const twee::OutputMode defaultOutMode = twee::OutputMode::HTML;
	const char* defaultStartName = "Start";
	const bool defaultTrimState = true;
	const char* defaultInputEncoding = "utf-8";
#endif
} // namespace

std::vector<std::filesystem::path> twee::environment::storyFormatDirs()
{
	const std::vector<std::filesystem::path> baseDirnames = {
		"storyformats",  ".storyformats",
		"story-formats", // DEPRECATED
		"storyFormats", // DEPRECATED
		"targets", // DEPRECATED
	};

	std::vector<std::filesystem::path> basePaths;

	appendExistingDirs(basePaths, splitDirList(TWINE_STORYFORMAT_DIRS));

	if (const auto homeDir = homeDirectory(); !homeDir.empty()) {
		appendExistingDirs(basePaths, homeDir, baseDirnames);
	}
	appendExistingDirs(basePaths, std::filesystem::current_path(), baseDirnames);

	// Merge values from the environment variables.
	appendDirsFormEnvronmentVariable(basePaths, "TWINE_STORYFORMATS");
	appendDirsFormEnvronmentVariable(basePaths, "TWEEGO_PATH");

	if (basePaths.empty()) { spdlog::critical("Story format search directories not found."); }

	return basePaths;
}
