/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_PASSAGE_HXX
#define TWEEXX_PASSAGE_HXX

#include "constants.hxx"
#include "types.hxx"

#include <cstdint>
#include <filesystem>
#include <iosfwd>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include "twee-export.h"

namespace twee {

	/**
	 * @todo write docs
	 */
	class TWEE_API Passage {
	public:
		struct Metadata {
			std::string position; // Unused by Tweego. Twine 1 & 2 passage block X and Y coordinates CSV.
			std::string size; // Unused by Tweego. Twine 2 passage block width and height CSV.
			std::uint64_t created; // created="201805052119"
			std::uint64_t modified; // modified="201805052119"
		};

		Passage() = default;
		Passage(std::string name);
		Passage(
			std::string name, std::filesystem::path sourceFilename, std::vector<std::string> tags, std::string source,
			Metadata meta = {});

		const std::string& name() const { return name_; }

		const std::string& text() const { return text_; }

		const std::filesystem::path& sourceFilename() const { return sourceFilename_; }

		void setSourceFilename(const std::filesystem::path& p);

		bool operator==(const Passage& other) const;

		bool tagsHas(string_view needle) const;
		bool tagsHasAny(const std::vector<string_view>& needles) const;
		bool tagsContains(string_view needle) const;
		bool tagsStartsWith(string_view needle) const;

		bool attributesHas(const string& needle) const;
		bool hasInfoTags() const;
		bool hasInfoName() const;
		bool isInfoPassage() const;
		bool isStoryPassage() const;
		bool hasAnyMetadata() const;

		void toTwee(std::ostream& os, TweeVersion version) const;
		void toPassagedata(std::ostream& os, std::size_t pid) const;
		void toTiddler(std::ostream& os, std::size_t pid) const;

		std::size_t countWords() const;

		const std::vector<std::string>& tags() const { return tags_; }

		void setTags(std::vector<std::string> tags);
		void setText(string_view text);
		void setText(std::string text);

		void setMetadata(Metadata&& m) { metadata_ = std::move(m); }

	private:
		using PassageAttr = std::map<std::string, std::string>;

		std::string marshalMetadata() const;

		std::string name_;
		std::vector<std::string> tags_;
		std::string text_;
		PassageAttr attributes_;
		Metadata metadata_;
		std::filesystem::path sourceFilename_;
	};

} // namespace twee

#endif // TWEEXX_PASSAGE_HXX
