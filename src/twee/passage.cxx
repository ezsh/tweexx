/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./passage.hxx"

#include "./escaping.hxx"

#include <boost/algorithm/string/join.hpp>

#include <algorithm>
#include <cctype>
#include <iomanip>
#include <ranges>
#include <regex>
#include <sstream>

namespace {
	using namespace std::string_view_literals;

	const std::vector<std::string_view> infoPassages = {
		// Twine 1.4+ vanilla story formats & SugarCube
		"StoryAuthor"sv,
		"StoryInit"sv,
		"StoryMenu"sv,
		"StorySubtitle"sv,
		"StoryTitle"sv,

		// SugarCube
		"PassageReady"sv,
		"PassageDone"sv,
		"PassageHeader"sv,
		"PassageFooter"sv,
		"StoryBanner"sv,
		"StoryCaption"sv,

		// SugarCube (v1 only)
		"MenuOptions"sv,
		"MenuShare"sv,
		"MenuStory"sv,

		// SugarCube (v2 only)
		"StoryShare"sv,

		// Compilers & Twine 1.4+ vanilla story formats
		"StorySettings"sv,

		// Twine 1.4+ & Twee2 compilers
		"StoryIncludes"sv,
	};
}

twee::Passage::Passage(std::string name)
	: Passage{std::move(name), {}, {}, std::string()}
{
}

twee::Passage::Passage(
	std::string name, std::filesystem::path sourceFilename, std::vector<std::string> tags, std::string source,
	Metadata meta)
	: name_{std::move(name)}
	, tags_{std::move(tags)}
	, text_{std::move(source)}
	, metadata_{std::move(meta)}
	, sourceFilename_{std::move(sourceFilename)}
{
}

void twee::Passage::setSourceFilename(const std::filesystem::path& p)
{
	sourceFilename_ = p;
}

bool twee::Passage::operator==(const twee::Passage& other) const
{
	return this->text_ == other.text_;
}

bool twee::Passage::tagsHas(string_view needle) const
{
	return std::ranges::any_of(tags_, [&](const std::string& t) { return t == needle; });
}

bool twee::Passage::tagsHasAny(const std::vector<string_view>& needles) const
{
	return std::ranges::any_of(needles, [this](string_view needle) { return this->tagsHas(needle); });
}

bool twee::Passage::tagsContains(string_view needle) const
{
	return std::ranges::any_of(
		tags_, [needle](const std::string& tag) { return tag.find(needle) != std::string::npos; });
}

bool twee::Passage::tagsStartsWith(string_view needle) const
{
	return std::ranges::any_of(tags_, [needle](const std::string& tag) { return tag.starts_with(needle); });
}

bool twee::Passage::attributesHas(const std::string& needle) const
{
	return attributes_.find(needle) != attributes_.end();
}

bool twee::Passage::hasInfoTags() const
{
	return tagsHasAny({"annotation"sv, "script"sv, "stylesheet"sv, "widget"sv}) || tagsStartsWith("Twine."sv);
}

bool twee::Passage::hasInfoName() const
{
	return std::ranges::contains(infoPassages, name());
}

bool twee::Passage::isInfoPassage() const
{
	return hasInfoName() || hasInfoTags();
}

bool twee::Passage::isStoryPassage() const
{
	return !hasInfoName() && !hasInfoTags();
}

void twee::Passage::toTwee(std::ostream& os, twee::TweeVersion version) const
{
	switch (version) {
		case TweeVersion::Twee1:
			os << ":: " << name();
			if (!tags_.empty()) { os << " [" << boost::algorithm::join(tags_, " ") << ']'; }
			break;
		case TweeVersion::Twee2:
			os << ":: " << name();
			if (!tags_.empty()) { os << " [" << boost::algorithm::join(tags_, " ") << ']'; }
			if (!metadata_.position.empty()) { os << '<' << metadata_.position << '>'; }
			break;
		case TweeVersion::Twee3:
			os << ":: " << tweeEscape(name());
			if (!tags_.empty()) { os << " [" << tweeEscape(boost::algorithm::join(tags_, " ")) << ']'; }
			if (hasAnyMetadata()) { os << ' ' << marshalMetadata(); }
			break;
	}

	os << '\n';
	if (!text_.empty()) { os << text_ << '\n'; }
	os << "\n\n";
}

void twee::Passage::toPassagedata(std::ostream& os, std::size_t pid) const
{
	std::string position;
	if (!metadata_.position.empty()) {
		position = metadata_.position;
	} else {
		// No position metadata, so generate something sensible on the fly.
		std::size_t x = pid % 10;
		std::size_t y = pid / 10;
		if (x == 0) {
			x = 10;
		} else {
			++y;
		}
		std::ostringstream posStream;
		posStream << x * 125 - 25 << ',' << y * 125 - 25;
		position = posStream.str();
	}
	std::string_view defaultSize = "100,100"sv;
	std::string_view size = !metadata_.size.empty() ? metadata_.size : defaultSize;

	/*
		<tw-passagedata pid="…" name="…" tags="…" position="…" size="…" source-file="…">…</tw-passagedata>
	*/
	auto sourcePath = sourceFilename();
	sourcePath.replace_extension();
	os << "<tw-passagedata pid=\"" << pid << "\" name=" << std::quoted(attrEscaped(name()))
	   << " tags=" << std::quoted(attrEscaped(boost::algorithm::join(tags_, " ")))
	   << ' ' << twee::passageMetadataKeys::position << '=' << std::quoted(attrEscaped(position))
	   << ' ' << twee::passageMetadataKeys::size << '=' << std::quoted(attrEscaped(size))
	   << ' ' << twee::passageMetadataKeys::sourceFile << '=' << std::quoted(sourcePath.generic_string())
	   << '>'
	   << htmlEscape(text()) << "</tw-passagedata>";
}

void twee::Passage::toTiddler(std::ostream& os, std::size_t pid) const
{
	// fake a twine-position attribute, if necessary
	std::string position;
	if (!metadata_.position.empty()) {
		position = metadata_.position;
	} else {
		// No position metadata, so generate something sensible on the fly.
		std::size_t x = pid % 10;
		std::size_t y = pid / 10;
		if (x == 0) {
			x = 10;
		} else {
			++y;
		}
		std::ostringstream posStream;
		posStream << x * 140 - 130 << ',' << y * 140 - 130;
		position = posStream.str();
	}

	/*
		<div tiddler="…" tags="…" created="…" modifier="…" twine-position="…">…</div>
	*/
	os << "<div tiddler=" << std::quoted(attrEscaped(name()))
	   << " tags=" << std::quoted(attrEscaped(boost::algorithm::join(tags_, " ")))
	   << " twine-position=" << std::quoted(attrEscaped(position)) << '>' << tiddlerEscape(text()) << "</div>";
}

std::size_t twee::Passage::countWords() const
{
	std::string text = text_;

	// Strip comments.
	const std::regex re = std::regex(R"(/%.*?%/|/\*.*?\*/|<!--.*?-->)");
	text = std::regex_replace(text, re, "");

	if (text.empty()) { return 0; }

	const char* str = text.c_str();
	bool inSpaces = true;
	std::size_t numWords = 0;

	while (*str != '\0') {
		if (std::isspace(*str)) {
			inSpaces = true;
		} else if (inSpaces) {
			numWords++;
			inSpaces = false;
		}

		++str;
	}

	return numWords;
}

bool twee::Passage::hasAnyMetadata() const
{
	return (!metadata_.position.empty() || !metadata_.size.empty());
}

std::string twee::Passage::marshalMetadata() const
{
	if (!hasAnyMetadata()) { return {}; }

	std::ostringstream res;
	res << "{";
	bool anyPropertyPrinted = false;
	if (!metadata_.position.empty()) {
		res << "\"position\":\"" << metadata_.position << '"';
		anyPropertyPrinted = true;
	}
	if (!metadata_.size.empty()) {
		if (anyPropertyPrinted) { res << ','; }
		res << "\"size\":\"" << metadata_.size << '"';
		anyPropertyPrinted = true;
	}
	res << '}';
	return res.str();
}

void twee::Passage::setTags(std::vector<std::string> tags)
{
	tags_ = std::move(tags);
}

void twee::Passage::setText(std::string text)
{
	text_ = std::move(text);
}

void twee::Passage::setText(string_view text)
{
	setText(std::string(text));
}
