/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./constants.hxx"

using namespace std::string_literals;
using namespace std::string_view_literals;

const std::string twee::defaultFormatID = "sugarcube-2"s;
const std::string_view twee::defaultFormatName = "SugarCube"sv;
const std::string_view twee::defaultFormatVersion = "2.0.0"sv;
const std::string_view twee::defaultOutFile = "-"sv;
// 	defaultOutMode   = outModeHTML
const std::string twee::defaultStartName = "Start"s;

const std::string_view twee::specialPassageNames::storyData = "StoryData"sv;
const std::string_view twee::specialPassageNames::storyIncludes = "StoryIncludes"sv;
const std::string_view twee::specialPassageNames::storyTitle = "StoryTitle"sv;
const std::string_view twee::specialPassageNames::storySettings = "StorySettings"sv;

const std::string_view twee::namedEncoding::utf8 = "UTF-8"sv;
