/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./ifid.hxx"

#include <boost/algorithm/string/case_conv.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <random>

namespace {
	template<class T>
	inline bool inRange(T t, T min, T max)
	{
		return t >= min && t <= max;
	}
} // namespace

std::string twee::newIFID()
{
	boost::uuids::basic_random_generator<std::mt19937> gen;
	boost::uuids::uuid uuid = gen();

	uuid.data[6] = (uuid.data[6] & 0x0f) | 0x40; // version 4
	uuid.data[8] = (uuid.data[8] & 0x3f) | 0x80; // variant 10

	return boost::lexical_cast<std::string>(uuid);
}

twee::IFIDValidationResult twee::validateIFID(std::string ifid)
{
	using namespace std::string_literals;

	switch (ifid.size()) {
		// xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
		case 36:
			// no-op
			break;
		// UUID://xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx//
		case 36 + 9:
			boost::algorithm::to_upper(ifid);
			if (ifid.find("UUID://") != 0 || ifid[43] != '/' || ifid[44] != '/') {
				return {false, "invalid IFID UUID://…// format"};
			}
			ifid = ifid.substr(7, 36);
			break;
		default:
			return {false, "invalid IFID length: "s + boost::lexical_cast<std::string>(ifid.size())};
	}

	for (std::size_t i = 0; i < ifid.size(); ++i) {
		const char c = ifid[i];
		switch (i) {
				// hyphens
			case 8:
			case 13:
			case 18:
			case 23:
				if (c != '-') {
					return {false,
							"invalid IFID character "s + c + " at position " + boost::lexical_cast<std::string>(i + 1)};
				}
				break;
			// version
			case 14:
				if ('1' > c || c > '5') {
					return {false, "invalid version "s + c + " at position " + boost::lexical_cast<std::string>(i + 1)};
				}
				break;
			// variant
			case 19:
				switch (c) {
					case '8':
					case '9':
					case 'a':
					case 'A':
					case 'b':
					case 'B':
						break;
					default:
						return {false,
								"invalid variant "s + c + " at position " + boost::lexical_cast<std::string>(i + 1)};
				}
				break;

			// regular hex character
			default:
				if (!inRange(c, '0', '9') && !inRange(c, 'a', 'f') && !inRange(c, 'A', 'F')) {
					return {false,
							"invalid IFID hex value "s + c + " at position " + boost::lexical_cast<std::string>(i + 1)};
				}
		}
	}

	return {true, {}};
}
