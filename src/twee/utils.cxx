/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./utils.hxx"

#include "./config.hxx"
#include "./formats.hxx"
#include "./impl/glob.hxx"
#include "./impl/private_utils.hxx"

#include "twee-config.h"

#include <algorithm>
#include <iostream>
#include <memory>
#include <regex>

std::vector<std::filesystem::path> twee::utils::collectFiles(
	const std::vector<std::filesystem::path>& includePatterns,
	const std::vector<std::filesystem::path>& excludePatterns)
{
	const auto collector = [](const std::filesystem::path& pattern) -> std::vector<std::filesystem::path> {
		const char* wildcards = "*?";
		const bool isPattern = pattern.string().find_first_of(wildcards) != std::string::npos;
		if (isPattern) { return twee::impl::glob(pattern); }
		if (!std::filesystem::exists(pattern)) {
			throw std::runtime_error(
				"Globbing template '" + pattern.string() + "' contains no wildcards and points to a non-existing path");
		}

		auto status = std::filesystem::status(pattern);
		if (std::filesystem::is_directory(status)) { return twee::impl::glob(std::filesystem::path(pattern) / "**"); }
		return {pattern};
	};

	std::vector<std::regex> excludeRegexes;
	std::ranges::transform(excludePatterns, std::back_inserter(excludeRegexes), [](const std::filesystem::path& p) {
		return std::regex(impl::regexFromGlobPattern(p.generic_string()));
	});

	std::vector<std::filesystem::path> res;
	for (const std::filesystem::path& ip: includePatterns) {
		auto includeFiles = collector(ip);
		if (excludePatterns.empty()) {
			std::copy(includeFiles.begin(), includeFiles.end(), std::back_inserter(res));
		} else {
			for (const std::filesystem::path& f: includeFiles) {
				for (const auto& ep: excludeRegexes) {
					if (!std::regex_match(f.string(), ep)) { res.push_back(f); }
				}
			}
		}
	}

	return res;
}

std::string_view twee::utils::versionString()
{
	return {APP_VER_STR};
}

void twee::utils::printAvailableStoryFormats(std::ostream& os, const twee::StoryFormatsMap* formats)
{
	std::unique_ptr<StoryFormatsMap> localMap;
	if (!formats) {
		localMap.reset(new StoryFormatsMap(environment::storyFormatDirs()));
		formats = localMap.get();
	}

	os << "Available story formats:" << std::endl << std::endl;
	auto oldFmt = os.setf(std::ios::left, std::ios::adjustfield);
	os << std::setw(16) << "Id" << std::setw(9) << "Version" << std::setw(16) << "Name"
	   << "Path" << std::endl;
	os << std::setfill('-') << std::setw(80) << '-' << std::setfill(' ') << std::endl;
	for (const auto& id: formats->ids()) {
		os << std::setw(16) << id;
		const auto& f = formats->getByID(id);
		os << std::setw(9) << f.versionStr() << std::setw(16) << f.name() << f.fileName();
		if (f.proofing()) { std::cout << " [proofing]"; }
		os << std::endl;
	}
	os.setf(oldFmt);
}
