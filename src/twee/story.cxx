/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./story.hxx"

#include "./constants.hxx"
#include "./escaping.hxx"
#include "./impl/private_utils.hxx"
#include "./storymetadata.hxx"
#include "./types.hxx"

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <fmt/ostream.h>
#include <spdlog/spdlog.h>

#include <ctime>
#include <stdexcept>
#include <string_view>

namespace {
	namespace sourceFileExtensions {
		using namespace std::string_view_literals;
		using ExtSet = std::set<std::string_view>;

		const ExtSet twee = {"tw"sv, "twee"sv};
		const ExtSet twee2 = {"tw2"sv, "twee2"sv};
		const ExtSet html = {"htm"sv, "html"sv};
		const ExtSet css = {"css"sv};
		const ExtSet js = {"js"sv};
		const ExtSet fonts = {"otf"sv, "ttf"sv, "woff"sv, "woff2"sv};
		const ExtSet images = {"gif"sv, "jpeg"sv, "jpg"sv, "png"sv, "svg"sv, "tif"sv, "tiff"sv, "webp"sv};
		const ExtSet audios = {"aac"sv, "flac"sv, "m4a"sv, "mp3"sv, "oga"sv, "ogg"sv, "opus"sv, "wav"sv, "wave"sv, "weba"sv};
		const ExtSet videos = {"mp4"sv, "ogv"sv, "webm"sv};
		const ExtSet vtt = {"vtt"sv};
	} // namespace sourceFileExtensions
} // namespace

twee::Story::Story(string name)
	: name_{std::move(name)}
{
}

twee::Story::Story(
	const std::vector<std::filesystem::path>& filenames, const twee::StoryLoadingOptions& options,
	const std::vector<std::filesystem::path>& modules)
	: baseDir_{options.baseDir}
{
	for (const std::filesystem::path& filename: filenames) {
		if (processed_.count(filename)) {
			spdlog::warn("load '{}': Skipping duplicate.", filename.string());
			continue;
		}

		spdlog::trace("loading story file {}", fmt::streamed(filename));

		const auto fileExt = impl::normalizedFileExt(filename);
		namespace sfe = sourceFileExtensions;
		try {
			if (sfe::twee.contains(fileExt)) {
				loadTwee(filename, options.encoding, options.trim, options.twee2Compat);
			} else if (sfe::twee2.contains(fileExt)) {
				loadTwee(filename, options.encoding, options.trim, true);
			} else if (sfe::css.contains(fileExt)) {
				loadTagged("stylesheet", filename, options.encoding);
			} else if (sfe::js.contains(fileExt)) {
				loadTagged("script", filename, options.encoding);
			} else if (sfe::fonts.contains(fileExt)) {
				loadFont(filename);
			} else if (sfe::images.contains(fileExt)) {
				loadMedia("Twine.image", filename);
			} else if (sfe::audios.contains(fileExt)) {
				loadMedia("Twine.audio", filename);
			} else if (sfe::videos.contains(fileExt)) {
				loadMedia("Twine.video", filename);
			} else if (sfe::vtt.contains(fileExt)) {
				loadMedia("Twine.vtt", filename);
			} else {
				// Simply ignore all other file types.
				SPDLOG_TRACE("ignoring {}", filename.string());
				continue;
			}
		} catch (std::runtime_error& ex) {
			spdlog::critical("error loading {}: ", fmt::streamed(filename), ex.what());
		}

		processed_.insert(filename);
	}

	/*
		Postprocessing.
	*/

	// Prepend the `StoryTitle` special passage, if necessary.
	if (!name().empty() && !contains(specialPassageNames::storyTitle)) {
		spdlog::debug("Prepending StoryTitle passage");
		push_front({string(specialPassageNames::storyTitle), {}, {}, name()});
	}

	addModules(modules, options.encoding);

	// 	selectFormat(c);
}

bool twee::Story::contains(string_view name) const
{
	return std::ranges::any_of(passages_, [&](const Passage& p) { return p.name() == name; });
}

#if 0
std::size_t twee::Story::index(const std::string& name) const
{
	const auto it =
		std::find_if(passages_.begin(), passages_.end(), [&](const Passage& p) { return p.name() == name; });
	return it == passages_.end() ? InvalidIndex : std::distance(passages_.begin(), it);
}

void twee::Story::deleteAt(std::size_t index)
{
	if (index > passages_.size()) {
		std::ostringstream errMsg;
		errMsg << "deleteAt " << index << ": Index out of range.";
		throw std::out_of_range(errMsg.str());
	}

	std::move(passages_.begin() + index + 1, passages_.end(), passages_.begin() + index);
	passages_.resize(passages_.size() - 1);
}
#endif

void twee::Story::push_back(twee::Passage&& p)
{
	// pre-processing
	if (p.name() == specialPassageNames::storyIncludes) {
		/*
			NOTE: StoryIncludes is a compiler special passage for Twine 1.4,
			and apparently Twee2.  Twee 1.4 does not support it—likely for
			the same reasons Tweego will not (outlined below).

			You may specify an arbitrary number of files and directories on
			the the command line for Tweego to process.  Furthermore, it will
			search all directories encountered during processing looking for
			additional files and directories.  Thus, supporting StoryIncludes
			would be beyond pointless.

			If we see StoryIncludes, log a warning and ignore it.
		*/
		spdlog::warn(R"(Ignoring "StoryIncludes" compiler special passage.  With Tweego you
		 should simply specify the files and directories for processing on the
		 command line.  In practice, you only need to specify your project's
		 root directory, as Tweego will search directories looking for files.)");
	} else if (p.name() == specialPassageNames::storyData) {
		loadStoryData(metadata(), p);
	} else if (p.name() == specialPassageNames::storyTitle) {
		const auto& pText = p.text();
		if (pText.size() < 255 && pText == htmlEscaped(pText)) {
			setName(p.text());
		} else {
			spdlog::warn("{} passage contains HTML notation or too long text, story name left unchanged.", p.name());
		}
	} else if (p.name() == specialPassageNames::storySettings) {
		std::vector<std::string> lines;
		boost::algorithm::split(
			lines, p.text(), [](char c) { return c == '\n'; }, boost::token_compress_on);
		for (auto& line: lines) {
			boost::algorithm::trim(line);
			const auto colonPos = line.find(':');
			if (colonPos == std::string::npos) {
				if (!line.empty()) { spdlog::warn("Malformed \"StorySettings\" entry; skipping {}", line); }
				continue;
			}

			std::string key = line.substr(0, colonPos);
			std::string val = line.substr(colonPos + 1);
			settings_[boost::algorithm::trim_copy(key)] = boost::algorithm::trim_copy(val);
		}
	}

	// append the passage if new, elsewise replace the existing version
	const auto it =
		std::find_if(passages_.begin(), passages_.end(), [&](const Passage& sp) { return p.name() == sp.name(); });
	if (it == passages_.end()) {
		passages_.push_back(std::move(p));
	} else {
		spdlog::warn("Replacing existing passage \"{}\" with duplicate.", p.name());
		*it = std::move(p);
	}
}

void twee::Story::push_back(const twee::Passage& passage)
{
	push_back(Passage(passage));
}

bool twee::Story::settingsHas(const string& name) const
{
	return settings_.contains(name);
}

void twee::Story::setName(string_view name)
{
	name_ = name;
}

// Statistics are collected statistics about the compiled story (e.g. passage counts, word counts, etc.).
twee::StoryStatistics twee::Story::statistics() const
{
	StoryStatistics res;
	std::set<std::filesystem::path> projectFiles;
	res.counts.passages = res.counts.storyPassages = res.counts.storyWords = 0;
	for (const auto& p: passages_) {
		++res.counts.passages;
		if (p.isStoryPassage()) {
			++res.counts.storyPassages;
			res.counts.storyWords += p.countWords();
		}
		if (!projectFiles.contains(p.sourceFilename())) {
			res.files.project.push_back(p.sourceFilename());
			projectFiles.insert(p.sourceFilename());
		}
	}
	for (const auto& f: modules_) {
		res.files.external.push_back(f.first);
	}

	return res;
}

twee::Passage twee::Story::createStoryTitlePassage() const
{
	return {std::string(specialPassageNames::storyTitle), {}, {}, this->name()};
}

void twee::Story::push_front(twee::Passage&& passage)
{
	passages_.push_front(std::move(passage));
}

void twee::Story::push_front(const twee::Passage& passage)
{
	push_front(Passage(passage));
}

void twee::Story::addModule(const std::filesystem::path& filename, string_view encoding)
{
	modules_.emplace_back(filename, encoding);
}
