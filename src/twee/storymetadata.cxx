/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./storymetadata.hxx"

#include "./constants.hxx"
#include "./ifid.hxx"
#include "./impl/json.hxx"
#include "./passage.hxx"

#include <boost/algorithm/string/case_conv.hpp>
#include <spdlog/spdlog.h>

#include <cmath>
#include <sstream>
#include <utility>

void twee::loadStoryData(twee::StoryMetadata& metadata, const twee::Passage& storyData)
{
	namespace json = impl::json;

	json::JsonObject root;
	std::istringstream is{storyData.text()};
	json::readFromStream(is, root);

	using json::getValue;

	metadata.ifid = boost::algorithm::to_upper_copy(getValue(root, "ifid", metadata.ifid));
	auto& tw2 = metadata.twine2;
	tw2.format = getValue(root, "format", tw2.format);
	tw2.formatVersion = getValue(root, "format-version", tw2.format);
	tw2.start = getValue(root, "start", tw2.start);
	tw2.zoom = getValue(root, "zoom", tw2.zoom);

	const json::JsonObject tagColors = getValue(root, "tag-colors", json::JsonObject());
	json::forEachMember(tagColors, [&](const std::string& color, const json::JsonObject& v){
		tw2.tagColors.insert({color, json::objValueAsString(v)});
	});

	const auto& [ok, errorMessage] = validateIFID(metadata.ifid);
	if (!ok) { spdlog::error("Invalid IFID read from '{}' passage: {}", specialPassageNames::storyData, errorMessage); }
}

twee::Passage twee::createStoryDataPassage(const twee::StoryMetadata& metadata)
{
	namespace json = impl::json;

	Passage res{string(specialPassageNames::storyData)};

	json::JsonObject root;
	const auto& tw2 = metadata.twine2;

	if (!metadata.ifid.empty()) {json::putValue(root, "ifid", metadata.ifid);}
	if (!tw2.format.empty()) {json::putValue(root, "format", tw2.format);}
	if (!tw2.formatVersion.empty()) {json::putValue(root, "format-version", tw2.formatVersion);}
	if (!tw2.start.empty()) { json::putValue(root, "start", tw2.start); }
	if (!tw2.tagColors.empty()) {
		json::JsonObject tags;
		for (const auto& tc: tw2.tagColors) {
			json::putValue(tags, tc.first.c_str(), tc.second);
		}
		json::putValue(root, "tag-colors", tags);
	}
	if (std::abs(tw2.zoom - 1.) > 1e-2) { // percent should be enough for everyone :D
		json::putValue(root, "zoom", tw2.zoom);
	}

	std::ostringstream text;
	json::writeToStream(text, root);
	res.setText(text.str());
	return res;
}
