/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./logging.hxx"

#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#	include <spdlog/sinks/msvc_sink.h>
#endif

#include <memory>

std::shared_ptr<spdlog::logger> twee::logging::setupLogger(const std::vector<spdlog::sink_ptr>& sinks)
{
	if (!sinks.empty()) {
		auto logger = std::make_shared<spdlog::logger>("twee", std::begin(sinks), std::end(sinks));
		spdlog::register_logger(logger);
		spdlog::set_default_logger(logger);
		return logger;
	} else {
		return spdlog::stdout_color_mt("twee");
	}
}

std::shared_ptr<spdlog::logger> twee::logging::setupLogger(const std::filesystem::path& logFile, bool consoleLog)
{
	std::vector<spdlog::sink_ptr> sinks;
	if (consoleLog) { sinks.push_back(std::make_shared<spdlog::sinks::stderr_color_sink_mt>()); }
	if (!logFile.empty()) { sinks.push_back(std::make_shared<spdlog::sinks::basic_file_sink_mt>(logFile.string())); }
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
	sinks.push_back(std::make_shared<spdlog::sinks::msvc_sink_mt>());
#endif
	return setupLogger(sinks);
}
