/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./formats.hxx"

#include "./impl/json.hxx"
#include "./io.hxx"
#include "./version.hxx"

#include <boost/algorithm/string/case_conv.hpp>
#include <fmt/ostream.h>
#include <spdlog/spdlog.h>

#include <algorithm>
#include <sstream>
#include <string_view>

namespace {
	struct Twine2Format {
		std::string name;
		std::string version;
		std::string description;
		std::string author;
		std::string image;
		std::string url;
		std::string license;
		std::string source;
		bool proofing;
		// Setup       string `json:"-"`
	};

	Twine2Format getStoryFormatData(
		const std::string& src, const twee::StoryFormat& f, bool harloweTrickApplied = false)
	{
		if (!f.isTwine2Style()) { throw std::logic_error("Not a Twine 2 style story format."); }

		// get the JSON chunk from the source
		// the file is a function call with the argument we are interested in
		// example: window.storyFormat({<JSON>});
		std::string source;
		if (!harloweTrickApplied) {
			const char opening[] = "({";
			const char closing[] = "})";
			auto first = src.find(opening);
			auto last = src.rfind(closing);
			auto jsonChunkLen = last - first;
			if (first == src.npos || last == src.npos || jsonChunkLen < 2) {
				throw twee::FormatLoadingError(f, "Could not find Twine 2 style story format JSON chunk.");
			}
			assert(last != src.npos);
			source = src.substr(first + 1, jsonChunkLen);
		} else {
			source = src;
		}
		/*
			START Harlowe malformed JSON chunk workaround

			TODO: Remove this workaround that attempts to handle Harlowe's
			broken JSON chunk.

			NOTE: This workaround is only possible because, currently,
			Harlowe's "setup" property is the last entry in the chunk.
		*/
		if (!harloweTrickApplied && boost::algorithm::to_lower_copy(f.id()).find("harlowe") == 0) {
			auto i = source.rfind(",\"setup");
			if (i != source.npos) {
				// cut the "setup" property and its invalid value
				return getStoryFormatData(source.substr(0, i) + '}', f, true);
			}
			/*
				If we've reached this point, either the format is not Harlowe
				or we cannot find the start of its "setup" property, so just
				return the JSON decoding error as normal.

				END Harlowe malformed JSON chunk workaround
			*/

			throw twee::FormatLoadingError(f, "Could not decode story format JSON chunk.");
		}

		// parse the JSON
		Twine2Format data;
		namespace json = twee::impl::json;

		json::JsonObject root;
		std::istringstream in{source};
		json::readFromStream(in, root);

		data.name = json::getValue(root, "name", std::string());
		data.version = json::getValue(root, "version", std::string());
		data.description = json::getValue(root, "description", std::string());
		data.author = json::getValue(root, "author", std::string());
		data.image = json::getValue(root, "image", std::string());
		data.url = json::getValue(root, "url", std::string());
		data.license = json::getValue(root, "license", std::string());
		data.proofing = json::getValue(root, "proofing", false);
		data.source = json::getValue(root, "source", std::string());
		return data;
	}
} // namespace

twee::StoryFormat::StoryFormat() = default;

twee::StoryFormat::StoryFormat(const std::string& id, const std::filesystem::path& filename, bool isTwine2)
	: id_{id}
	, filename_{filename}
	, twine2_{isTwine2}
{
    unmarshalMetadata();
}

void twee::StoryFormat::unmarshalMetadata()
{
	if (!isTwine2Style()) { return; }

	// read in the story format
	std::string fSource = io::readAll(filename_);
	Twine2Format data = getStoryFormatData(fSource, *this);
	name_ = data.name;
	version_ = data.version;
	proofing_ = data.proofing;
}

std::string twee::StoryFormat::source() const
{
	try {
		std::string source = io::readAll(fileName());
		return isTwine2Style() ? getStoryFormatData(source, *this).source : source;
	} catch (std::runtime_error& ex) {
		spdlog::error("format {} error: {}", name_, ex.what());
	}

	return {};
}

twee::StoryFormatsMap::StoryFormatsMap(const std::vector<std::filesystem::path>& searchPaths)
{
	std::vector<std::string> baseFilenames = {"format.js", "header.html"};

	for (const auto& searchDirname: searchPaths) {
		if (!std::filesystem::is_directory(searchDirname)) { continue; }

		for (const auto& formatDirname: std::filesystem::directory_iterator(searchDirname)) {
			if (!std::filesystem::is_directory(formatDirname)) { continue; }

			for (const auto& baseFilename: baseFilenames) {
				const std::filesystem::path formatFilename = formatDirname.path() / baseFilename;
				if (std::filesystem::is_regular_file(formatFilename)) {
					try {
						const std::string id = formatFilename.parent_path().stem().string();
						map_[id] = StoryFormat(id, formatFilename, baseFilename == "format.js");
					} catch (std::runtime_error& ex) {
						spdlog::warn("Skipping format {} because of error: {}", fmt::streamed(formatFilename.filename()),
									 ex.what());
						continue;
					}
					break;
				}
			}
		}
	}

	if (map_.empty()) {
		spdlog::error("Story formats not found within the search directories: (in order)");
		for (std::size_t i = 0; i < searchPaths.size(); ++i) { spdlog::error("{}. {}", i, searchPaths[i].string()); }
		throw NoStoryFormatsFound(searchPaths);
	}
}

bool twee::StoryFormatsMap::isEmpty() const
{
	return map_.empty();
}

bool twee::StoryFormatsMap::hasByID(std::string_view id) const
{
	return map_.count(id);
}

bool twee::StoryFormatsMap::hasByTwine2Name(std::string_view name) const
{
	return map_.count(getIDByTwine2Name(name));
}

bool twee::StoryFormatsMap::hasByTwine2NameAndVersion(std::string_view name, std::string_view version) const
{
	return map_.count(getIDByTwine2NameAndVersion(name, version));
}

std::string twee::StoryFormatsMap::getIDByTwine2Name(std::string_view name) const
{
	// 	var (
	// 		found *semver.Version
	// 		id    string
	// 	)

	for (const auto& fp: map_) {
		const StoryFormat& f = fp.second;
		if (!f.isTwine2Style()) { continue; }

		if (f.name() == name) {
			return fp.first;
#if 0
			if v, err := semver.ParseTolerant(f.version); err == nil {
				if found == nil || v.GT(*found) {
					found = &v
					id = f.id
				}
			}
#endif
		}
	}

	return {};
}

std::string twee::StoryFormatsMap::getIDByTwine2NameAndVersion(
	std::string_view name, std::string_view version) const
{
	using Version = utils::Version<unsigned, 3, 1>;

	Version found;
	std::string id;
	// 	var (
	// 		wanted *semver.Version
	// 		found  *semver.Version
	// 		id     string
	// 	)
	Version wanted;
	try {
		wanted = Version(version);
	} catch (std::runtime_error&) {
		spdlog::warn("format '{}': Auto-selecting greatest version; Could not parse version '{}'", name, version);
		wanted = {0u, 0u, 0u};
	}

	for (const auto& fp: map_) {
		const StoryFormat& f = fp.second;
		if (!f.isTwine2Style()) { continue; }

		if (f.name() == name) {
			try {
				Version v = f.versionStr();
				if (wanted.majorNumber() == 0 || (v.majorNumber() == wanted.majorNumber() && v >= wanted)) {
					if (v > found) {
						found = v;
						id = f.id();
					}
				}
			} catch (...) {
			}
		}
	}

	return id;
}

const twee::StoryFormat& twee::StoryFormatsMap::getByID(std::string_view id) const
{
	const auto i = map_.find(id);
	if (i == map_.end()) { throw FormatNotFound(fmt::format("for id '{}'", id)); }
	return i->second;
}

const twee::StoryFormat& twee::StoryFormatsMap::getByTwine2Name(std::string_view name) const
{
	try {
		return getByID(getIDByTwine2Name(name));
	} catch (FormatNotFound&) {
		throw FormatNotFound(fmt::format("for name '{}'", name ));
	}
}

const twee::StoryFormat& twee::StoryFormatsMap::getByTwine2NameAndVersion(
	std::string_view name, std::string_view version) const
{
	try {
		return getByID(getIDByTwine2NameAndVersion(name, version));
	} catch (FormatNotFound&) {
		throw FormatNotFound(fmt::format("for name '{}' and version '{}'", name, version));
	}
}

std::vector<std::string> twee::StoryFormatsMap::ids() const
{
	std::vector<std::string> res;
	res.reserve(map_.size());
	std::ranges::transform(map_, std::back_inserter(res), [](const auto& p) { return p.first; });
	return res;
}

twee::FormatNotFound::FormatNotFound(const std::string& message)
	: std::runtime_error("Could not find story format " + message)
{
}

twee::FormatLoadingError::FormatLoadingError(const twee::StoryFormat& format, const std::string& message)
    : std::runtime_error(
          fmt::format("Error while loading story format '{}' from {}: {}", format.name(), fmt::streamed(format.fileName()), message))
{
}

twee::NoStoryFormatsFound::NoStoryFormatsFound(const std::vector<std::filesystem::path>& searchDirs)
	: std::runtime_error("Could not find story formats in the search paths")
	, searchDirerctories{searchDirs}
{
}
