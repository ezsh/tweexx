/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_IMPL_GLOB_HXX
#define TWEEXX_IMPL_GLOB_HXX

#include <filesystem>
#include <vector>

namespace twee::impl {
	std::vector<std::filesystem::path> glob(std::filesystem::path pattern, bool includeDirs = false);
}

#endif
