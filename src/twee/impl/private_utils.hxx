/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEE_IMPL_PRIVATE_UTILS_HXX
#define TWEE_IMPL_PRIVATE_UTILS_HXX

#include "../types.hxx"

#include <cstdint>
#include <filesystem>
#include <iosfwd>
#include <string_view>
#include <vector>

namespace twee {
	class Passage;
	enum class TweeVersion: std::uint8_t;
}

namespace twee::impl {
	std::string toTitleCase(std::string s, const std::locale& loc = std::locale());
	std::string normalizedFileExt(const std::filesystem::path& filename);
	std::string_view mimeTypeFromFilename(const std::filesystem::path& filename);
	std::string_view mimeTypeFromExt(std::string_view ext);
	std::string_view extForMimeType(std::string_view mimeType);

	std::string regexFromGlobPattern(const std::string& globPattern);

	bool isMediaPassage(const Passage& passage);
	std::string_view mimeExt(const Passage& passage);
	void serializePassage(const Passage& passage, std::ostream& os, TweeVersion version, bool decode);
	std::string_view extForVersion(TweeVersion version);
	std::vector<string_view> split(string_view str, string_view::value_type delimiter);


} // namespace twee::impl

#endif
