/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./cfile.hxx"

#include <cstdint>
#include <cstdio>
#include <stdexcept>

#ifdef WIN32
#include <cstdlib>
using mode_char_t = wchar_t;
#define _TC(a) La
#else
using mode_char_t = char;
#define _TC(a) a
#endif

namespace {
	const mode_char_t* openmode2fopenmode(std::ios_base::openmode mode)
	{
		// shamelessly copied from the libstdc++ project
		// does not handle noreplace (c++23)
		enum : std::uint8_t {
			app = std::ios_base::app,
			binary = std::ios_base::binary,
			in = std::ios_base::in,
			out = std::ios_base::out,
			trunc = std::ios_base::trunc,
		};

		// see table at https://en.cppreference.com/w/cpp/io/basic_filebuf/open
		// clang-format off
		switch (mode & (binary|in|out|trunc|app))
		{
			case (   out                 ): return _TC("w");
			case (   out      |app       ):
			case (             app       ): return _TC("a");
			case (   out|trunc           ): return _TC("w");
			case (in                     ): return _TC("r");
			case (in|out                 ): return _TC("r+");
			case (in|out|trunc           ): return _TC("w+");
			case (in|out      |app       ):
			case (in          |app       ): return _TC("a+");
				
			case (   out          |binary): return _TC("wb");
			case (   out      |app|binary):
			case (             app|binary): return _TC("ab");
			case (   out|trunc    |binary): return _TC("wb");
			case (in              |binary): return _TC("rb");
			case (in|out          |binary): return _TC("r+b");
			case (in|out|trunc    |binary): return _TC("w+b");
			case (in|out      |app|binary):
			case (in          |app|binary): return _TC("a+b");
				
			default: return _TC(""); // invalid
		}
		// clang-format on
	}
}

io::file::file(const std::filesystem::path& filename, std::ios_base::openmode mode)
{
#ifndef WIN32
	f_ = std::fopen(filename.c_str(), openmode2fopenmode(mode));
#else
	f_ = ::_wfopen(filename.c_str(), openmode2fopenmode(mode));
#endif
	std::error_code ec;
	if (!f_) throw std::system_error(fail(ec));
}

io::file::file(io::file&& other) noexcept
	: f_{other.f_}
{
	other.f_ = nullptr;
}

io::file& io::file::operator=(io::file&& other) noexcept
{
	close();
	f_ = other.f_;
	other.f_ = nullptr;
	return *this;
}

io::file::~file()
{
	if (f_) std::fclose(f_);
}

long io::file::size() const
{
	const auto pos = std::ftell(f_);
	std::error_code ec;
	if (std::fseek(f_, 0, SEEK_END) != 0) throw std::system_error(fail(ec));
	long size = std::ftell(f_);
	if (size == -1) {
		size = 0;
		throw std::system_error(fail(ec));
	}
	if (std::fseek(f_, pos, SEEK_SET) != 0) throw std::system_error(fail(ec));
	return size;
}

std::error_code& io::file::fail(std::error_code& ec)
{
	ec.assign(errno, std::system_category());
	return ec;
}

[[noreturn]]
void io::file::fail()
{
	throw std::system_error(errno, std::system_category());
}

std::size_t io::file::read(void* data, std::size_t size, std::error_code& ec)
{
	auto const nread = std::fread(data, 1, size, f_);
	if (std::ferror(f_)) ec.assign(errno, std::system_category());
	return nread;
}

std::size_t io::file::read(void* data, std::size_t size)
{
	std::error_code ec;
	auto const nread = read(data, size, ec);
	if (ec) throw std::system_error(ec);
	return nread;
}

std::size_t io::file::write(const void* data, std::size_t size, std::error_code& ec)
{
	auto const nwritten = std::fwrite(data, 1, size, f_);
	if (std::ferror(f_)) ec.assign(errno, std::system_category());
	return nwritten;
}

std::size_t io::file::write(const void* data, std::size_t size)
{
	std::error_code ec;
	auto const nwritten = write(data, size, ec);
	if (ec) throw std::system_error(ec);
	return nwritten;
}

void io::file::seek(long offset, std::ios_base::seekdir origin)
{
	const auto fseekDir = [](std::ios_base::seekdir dir) {
		switch (dir) {
			case std::ios_base::beg: return SEEK_SET;
			case std::ios_base::cur: return SEEK_CUR;
			case std::ios_base::end: return SEEK_END;
			default: throw std::logic_error("Unexpected std::ios_base::seekdir value");
		}
	};
	if (std::fseek(f_, offset, fseekDir(origin)) != 0) fail();
}

void io::file::close()
{
	if (f_) {
		std::fclose(f_);
		f_ = nullptr;
	}
}
