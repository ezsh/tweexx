/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./sources_split.hxx"

#include "../constants.hxx"
#include "../passage.hxx"
#include <spdlog/spdlog.h>
#include "./private_utils.hxx"

#include <algorithm>
#include <cassert>
#include <filesystem>
#include <regex>
#include <string_view>

namespace {
	using namespace std::string_view_literals;
	using twee::Passage;
	using twee::string;

	using twee::impl::string_view;

	namespace typeTags {
		constexpr std::string_view style{"stylesheet"};
		constexpr std::string_view script{"script"};
		constexpr std::string_view image{"Twine.image"};
		constexpr std::string_view audio{"Twine.audio"};
		constexpr std::string_view video{"Twine.video"};
		constexpr std::string_view vtt{"Twine.vtt"};
	}

	namespace filenameExtensions {
		constexpr std::string_view style = ".css"sv;
		constexpr std::string_view script = ".js"sv;
	}

	std::vector<string_view> mergeShortGroups(const std::vector<string_view>& split, unsigned minSize)
	{
		if (split.size() < 2) { return split; }

		std::vector<string_view> res;
		std::size_t firstToMerge = twee::InvalidIndex;
		for (std::size_t i = 0; i < split.size(); ++i) {
			const string_view& v = split[i];
			if (v.length() >= minSize) {
				res.push_back(v);
			} else {
				if (firstToMerge == twee::InvalidIndex) {
					firstToMerge = i;
					continue;
				}
				const std::size_t accumulatedSize = static_cast<std::size_t>(v.cend() - split[firstToMerge].cbegin());
				if (accumulatedSize > minSize) {
					res.emplace_back(split[firstToMerge].data(), accumulatedSize);
					firstToMerge = i + 1;
				}
			}
		}
		if (firstToMerge < split.size()) {
			res.emplace_back(split[firstToMerge].data(), split.back().cend() - split[firstToMerge].cbegin());
		}
		return res;
	}

	std::string_view fileExtentionForPassage(const twee::Passage& passage, twee::TweeVersion version)
	{
		if (passage.tagsHas(typeTags::style)) {
			return filenameExtensions::style;
		}
		if (passage.tagsHas(typeTags::script)) {
			return filenameExtensions::script;
		}
		if (passage.tagsHas(typeTags::image)) {
			return twee::impl::mimeExt(passage);
		}
		if (passage.tagsHas(typeTags::audio)) {
			return twee::impl::mimeExt(passage);
		}
		if (passage.tagsHas(typeTags::video)) {
			return twee::impl::mimeExt(passage);
		}
		if (passage.tagsHas(typeTags::vtt)) {
			return twee::impl::mimeExt(passage);
		}
		return twee::impl::extForVersion(version);
	}

	std::filesystem::path fileNameForPassage(
		const std::filesystem::path& dir, const twee::Passage& passage, twee::TweeVersion version,
		const std::filesystem::path& subPath)
	{
		using namespace std::string_view_literals;

		const std::filesystem::path audioDir = dir / "audio"sv;
		const std::filesystem::path imagesDir = dir / "images"sv;
		const std::filesystem::path videoDir = dir / "video"sv;
		// const fs::path fontsDir = dir / "fonts"sv;
		const std::filesystem::path stylesDir = dir / "styles"sv;
		const std::filesystem::path scriptsDir = dir / "scripts"sv;
		const std::filesystem::path vttDir = dir / "vtt"sv;
		const std::filesystem::path tweeDir = dir / "twee"sv;

		std::filesystem::path res;

		if (passage.tagsHas(typeTags::style)) { // TODO handle embedded fonts
			res = stylesDir;
		} else if (passage.tagsHas(typeTags::script)) {
			res = scriptsDir;
		} else if (passage.tagsHas(typeTags::image)) {
			res = imagesDir;
		} else if (passage.tagsHas(typeTags::audio)) {
			res = audioDir;
		} else if (passage.tagsHas(typeTags::video)) {
			res = videoDir;
		} else if (passage.tagsHas(typeTags::vtt)) {
			res = vttDir;
		} else {
			res = tweeDir;
		}

		res /= subPath;

		res.replace_extension(fileExtentionForPassage(passage, version));
		return res;
	}
}

twee::impl::PassageDir twee::impl::makeSplit(
	const std::vector<const Passage*>& passages, const twee::string& storyName, const twee::SourceSplitOptions& opt)
{
	PassageDir res;

	const std::regex splitRx{opt.delimiterRegex()};
	const std::sregex_token_iterator end;

	for (const Passage* p: passages) {
		const auto& pName = p->name();
		const bool mediaPassage = isMediaPassage(*p);
		if (opt.split()) {
			std::vector<string_view> split;
			if (opt.preserveSourceInfo() && !p->sourceFilename().empty()) {
				// this will disable merges, but we have to remember what that means when saving...
				res.addEntry({p, {}});
			} else {
				std::sregex_token_iterator iter{pName.begin(), pName.end(), splitRx, -1};
				for (; iter != end; ++iter) {
					const std::size_t start = static_cast<std::size_t>(std::distance(pName.begin(), iter->first));
					split.emplace_back(&pName[start], iter->length());
					if (opt.maxDepth() > 0 && split.size() >= static_cast<unsigned>(opt.maxDepth())) {
						split.back() = {&pName[start], pName.length() - start};
						break;
					}
				}

				// now we merge short fragments
				std::vector<string_view> mergedSplit = mergeShortGroups(split, opt.minFragmentLength());

				if (mediaPassage) { mergedSplit.back() = pName; }

				PassageEntry pe{p, std::move(mergedSplit)};
				res.addEntry(std::move(pe));
			}
		} else if (opt.extractMedia() && mediaPassage) {
			std::vector<string_view> name = {pName};
			res.addEntry({p, std::move(name)});
		} else {
			std::vector<string_view> name = {storyName};
			res.addEntry({p, std::move(name)});
		}
	}

	return res;
}

std::map<const Passage*, std::filesystem::path> twee::impl::PassageDir::flattern(
	const std::filesystem::path& rootDir, twee::TweeVersion version) const
{
	std::map<const Passage*, std::filesystem::path> res;

	const auto addDir = [&res, &rootDir, version](const PassageDir& directory) -> void {
		const auto& addDirImpl = [&res, &rootDir, version](const PassageDir& dir, const auto& self) -> void {
			for (const auto& d: dir.subdirs_) {
				self(*d.second, self);
			}
			for (const auto& p: dir.passages_) {
				if (p.split.empty()) {
					std::filesystem::path sourceFileName = p.passage->sourceFilename();
					if (!sourceFileName.has_extension()) {
						sourceFileName.replace_extension(fileExtentionForPassage(*p.passage, version));
					}
					res[p.passage] = sourceFileName;
				} else {
					res[p.passage] = fileNameForPassage(rootDir, *p.passage, version, p.split.toPath());
				}
			}
		};

		addDirImpl(directory, addDirImpl);
	};

	addDir(*this);
	return res;
}

twee::impl::StringSplit::StringSplit(std::vector<string_view> split)
	: split_{std::move(split)}
{
}

void twee::impl::StringSplit::merge(std::size_t index)
{
	// merge i-th with (i+1)-th
	assert(index + 1 < count());
	auto gap = static_cast<std::size_t>(split_[index + 1].data() - split_[index].data()) - split_[index].length();
	const auto indexIter = split_.begin() + static_cast<decltype(split_)::iterator::difference_type>(index);
	*indexIter = string_view{split_[index].data(), split_[index].length() + gap + split_[index + 1].length()};
	std::move(indexIter + 2, split_.end(), indexIter + 1);
	split_.resize(split_.size() - 1);
}

std::filesystem::path twee::impl::StringSplit::toPath() const
{
	std::filesystem::path res;
	for (const auto& part: split_) {
		res /= part;
	}
	return res;
}

twee::impl::PassageEntry::PassageEntry(const twee::Passage* p, std::vector<string_view>&& s)
	: passage{p}
	, split{s}
{
}

void twee::impl::PassageDir::ensureSubdir(const twee::string& dir)
{
	auto it = subdirs_.find(dir);
	if (it == subdirs_.end()) {
		subdirs_[dir] = std::make_unique<PassageDir>();
		subdirs_[dir]->level_ = this->level_ + 1;
	}
}

twee::impl::PassageDir& twee::impl::PassageDir::subdir(const twee::string& dir)
{
	return *subdirs_.at(dir);
}

void twee::impl::PassageDir::addEntry(twee::impl::PassageEntry pe)
{
	addEntry(std::move(pe), this->level_);
}

void twee::impl::PassageDir::addEntry(twee::impl::PassageEntry&& pe, std::size_t level)
{
	if (pe.split.empty() || (level == pe.split.count() - 1)) {
		passages_.push_back(pe);
		spdlog::debug("Adding passage w/o split with filename {}", pe.passage->sourceFilename().string());
	} else {
		const string str{pe.split[level].data(), pe.split[level].length()};
		ensureSubdir(str);
		subdirs_[str]->addEntry(std::move(pe), level + 1);
	}
}

void twee::impl::PassageDir::optmize(unsigned minDirSize)
{
	for (auto& de: subdirs_) {
		PassageDir& d = *de.second;
		d.optmize(minDirSize);
		if (d.passages_.size() < minDirSize) {
			const auto mergeLevel = this->level_;
			for (auto& pe: d.passages_) {
				pe.split.merge(mergeLevel);
				this->passages_.push_back(pe);
			}
			d.passages_.clear();
		}
	}
}
