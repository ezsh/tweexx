/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TWEEXX_TWEE2COMPAT_HXX
#define TWEEXX_TWEE2COMPAT_HXX

#include <string>

namespace twee::impl {
	bool hasTwee2Syntax(const std::string& s);

	// ToV3 returns a copy of the slice s with all instances of Twee2 position blocks
	// replaced with Twee v3 metadata blocks.
	std::string toV3(const std::string& s);
} // namespace twee::impl

#endif
