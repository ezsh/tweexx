/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_UTILS_HXX
#define TWEEXX_UTILS_HXX

#include <filesystem>
#include <iosfwd>
#include <string_view>
#include <vector>

#include "twee-export.h"

namespace twee {
	class StoryFormatsMap;
}

namespace twee::utils {
	TWEE_API std::vector<std::filesystem::path> collectFiles(
		const std::vector<std::filesystem::path>& includePatterns,
		const std::vector<std::filesystem::path>& excludePatterns);

	TWEE_API std::string_view versionString();
	TWEE_API void printAvailableStoryFormats(std::ostream& os, const StoryFormatsMap* formats = nullptr);
} // namespace twee::utils

#endif
