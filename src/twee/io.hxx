/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TWEEXX_IO_HXX
#define TWEEXX_IO_HXX

#include "./types.hxx"

#include <filesystem>
#include <iosfwd>

namespace twee::io {
	std::string readAllAsBase64(const std::filesystem::path& fileName);
	std::string readAll(const std::filesystem::path& fileName);
	std::string readAllWithEncoding(const std::filesystem::path& fileName, string_view enc);
	void writeAll(const std::filesystem::path& fileName, std::string_view data);

	void base64Encode(std::istream& is, std::ostream& os);
	void base64Decode(std::istream& is, std::ostream& os);
} // namespace twee::io

#endif
