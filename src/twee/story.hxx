/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TWEEXX_STORY_HXX
#define TWEEXX_STORY_HXX

#include "./config.hxx"
#include "./constants.hxx"
#include "./passage.hxx"
#include "./storymetadata.hxx"

#include <deque>
#include <filesystem>
#include <set>
#include <utility>

#include "twee-export.h"

namespace detail {
	template <typename Sample, typename T>
	struct apply_ref {
		using type = T;
	};

	template <typename Sample, typename T>
	struct apply_ref<Sample&, T> {
		using type = T&;
	};

	template <typename Sample, typename T>
	struct apply_ref<Sample&&, T> {
		using type = T&&;
	};

	template <typename Sample, typename T>
	struct apply_const {
		using type = T;
	};

	template <typename Sample, typename T>
	struct apply_const<const Sample, T> {
		using type = const T;
	};
} // namespace detail

template <typename Sample, typename T>
using apply_const_t = detail::apply_const<std::remove_reference_t<Sample>, T>::type;

template <typename Sample, typename T>
using like_t = detail::apply_ref<Sample, apply_const_t<Sample, T>>::type;

namespace twee {

	struct StoryStatistics {
		struct {
			std::vector<std::filesystem::path> project; // Project source files.
			std::vector<std::filesystem::path> external; // Both modules and the head file.
		} files;
		struct {
			std::size_t passages; // Count of all passages.
			std::size_t storyPassages; // Count of story passages.
			std::size_t storyWords; // Count of story passage "words" (typing measurement style).
		} counts;
	};

	class TWEE_API Story {
	public:
		Story(string name);
		Story(
			const std::vector<std::filesystem::path>& filenames, const StoryLoadingOptions& options = {},
			const std::vector<std::filesystem::path>& modules = {});

		void addSource(std::filesystem::path& filename, string_view encoding = namedEncoding::utf8);
		void addModule(const std::filesystem::path& filename, string_view encoding = namedEncoding::utf8);

		template<class Container>
		void addSources(const Container& filenames, string_view encoding = namedEncoding::utf8)
		{
			for (const auto& filename: filenames) { this->addSource(filename, encoding); }
		}

		template<class Container>
		void addModules(const Container& filenames, string_view encoding = namedEncoding::utf8)
		{
			for (const auto& filename: filenames) { this->addModule(filename, encoding); }
		}

		const std::string& name() const
		{
			return name_;
		}

		void setName(string_view name);

		std::size_t count() const
		{
			return passages_.size();
		}

		bool contains(string_view name) const;
		void push_back(const Passage& passage);
		void push_back(Passage&& passage);
		void push_front(const Passage& passage);
		void push_front(Passage&& passage);

		bool settingsHas(const string& name) const;

		StoryStatistics statistics() const;

		auto& passages() const {
			return passages_;
		}

		auto& modules() const { return modules_; }

		template <typename Self>
		like_t<Self, StoryMetadata> metadata(this Self&& self)
		{
			return std::forward_like<Self>(self.metadata_);
		}

		const std::filesystem::path& baseDir() const { return baseDir_; }

		Passage createStoryTitlePassage() const;

	private:
		// loading
		TWEE_NO_EXPORT void loadTwee(
			const std::filesystem::path& filename, string_view encoding, bool trim, bool twee2Compat);

		TWEE_NO_EXPORT void loadTagged(
			const std::string& tag, const std::filesystem::path& filename, string_view encoding);
		TWEE_NO_EXPORT void loadMedia(const std::string& tag, const std::filesystem::path& filename);
		TWEE_NO_EXPORT void loadFont(const std::filesystem::path& filename);

		std::string name_;
		std::deque<Passage> passages_;
		std::map<std::string, std::string> settings_;
		bool testMode_;

		// Twine 1 & 2 compiler metadata.
		StoryMetadata metadata_;

		std::filesystem::path baseDir_; //!< story sources root directory, similar to a project directory
		// paths are proximate to baseDir_
		std::vector<std::pair<std::filesystem::path, std::string>> modules_;

		// internal compiler data
		std::set<std::filesystem::path> processed_;
	};

} // namespace twee
#endif
