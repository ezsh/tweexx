/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEE_EXPORTER_HXX
#define TWEE_EXPORTER_HXX

#include "./constants.hxx"
#include "./types.hxx"

#include <filesystem>
#include <iosfwd>

#include "twee-export.h"

namespace twee {
	class Story;

	class TWEE_API SourceSplitOptions {
	public:
		using ThisType = SourceSplitOptions;

		SourceSplitOptions(bool extractMedia = false, bool split = false);

		ThisType& split(bool value);
		ThisType& extractMedia(bool value);
		ThisType& maxDepth(int value);
		ThisType& delimiterRegex(const string& value);
		ThisType& minFragmentLength(unsigned value);
		ThisType& minGroupSize(unsigned value);
		ThisType& preserveSourceInfo(bool value);

		bool split() const;
		bool extractMedia() const;
		int maxDepth() const;
		string delimiterRegex() const;
		unsigned minFragmentLength() const;
		unsigned minGroupSize() const;
		bool preserveSourceInfo() const;
	private:
		int maxDepth_;
		string delimiterRegex_;
		unsigned minFragmentLength_;
		unsigned minGroupSize_;
		bool extractMedia_;
		bool split_;
		bool preserveSourceInfo_;
	};

	TWEE_API void storyToTwee(std::ostream& os, const Story& story, TweeVersion version = TweeVersion::Latest);
	TWEE_API void storyToTwee(
		const std::filesystem::path& dir, const Story& story, const SourceSplitOptions& splitOptions,
		TweeVersion version = TweeVersion::Latest);

} // namespace twee

#endif
