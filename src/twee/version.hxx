/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEX_VERSION_HXX
#define TWEEX_VERSION_HXX

#include <boost/lexical_cast.hpp>

#include <array>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

namespace twee::utils {
	namespace impl {
		std::vector<std::string_view> split(std::string_view s);
		inline std::vector<std::string_view> split(const std::string& s)
		{
			return split(std::string_view(s));
		}
	}

	template<typename T, std::size_t N, std::size_t Mandatory = N>
	class Version {
		static_assert(N > 0, "The number of version components may not be smaller than 1");
		static_assert(
		    N >= Mandatory, "The number of mandatory components may not be larger than the total number of components");

	public:
		typedef T ComponentType;
		typedef Version<T, N, Mandatory> ThisType;

		constexpr Version()
		    : m_components{{}}
		{
		}

		constexpr Version(const ThisType& other) = default;

		template<typename... Other>
		constexpr Version(Other... components)
		    : m_components{{static_cast<T>(components)...}}
		{
		}

		/**
		 * @brief Creates version from string in format "x.y.z"
		 *
		 * @param version Version string in format "x.y.z"
		 * @throws std::runtime_error if parsing fails
		 */
		Version(const std::string& version)
		    : Version{impl::split(version)}
		{
		}

		/**
		 * @brief Creates version from string in format "x.y.z"
		 *
		 * @param version Version string in format "x.y.z"
		 * @throws std::runtime_error if parsing fails
		 */
		Version(std::string_view version)
		    : Version{impl::split(version)}
		{
		}

		constexpr ComponentType majorNumber() const
		{
			static_assert(N >= 1, "The number of version components is too small");
			return m_components[0];
		}

		constexpr ComponentType minorNumber() const
		{
			static_assert(N >= 2, "The number of version components is too small");
			return m_components[1];
		}

		constexpr ComponentType revisionNumber() const
		{
			static_assert(N >= 3, "The number of version components is too small");
			return m_components[2];
		}

		constexpr ComponentType patchNumber() const
		{
			static_assert(N >= 4, "The number of version components is too small");
			return m_components[3];
		}

		constexpr ComponentType operator[](const std::size_t i) const
		{
			return m_components.at(i);
		}

		operator std::string() const
		{
			// find the last one non-zero component
			std::size_t lastSignificantIndex = N - 1;
			while ((lastSignificantIndex > 0) && ((*this)[lastSignificantIndex] == 0)) --lastSignificantIndex;

			if (lastSignificantIndex + 1 < Mandatory) // lastSignificantIndex >= 0
				lastSignificantIndex = Mandatory - 1; // and Mandatory >= 1

			std::ostringstream res;
			res << (*this)[0];
			for (std::size_t i = 1; i <= lastSignificantIndex; ++i) res << '.' << (*this)[i];
			return res.str();
		}

		constexpr bool isValid() const
		{
			return (*this != ThisType{});
		}

		constexpr bool operator==(const ThisType& other) const
		{
			return (m_components == other.m_components);
		}

		constexpr bool operator<(const ThisType& other) const
		{
			return (m_components < other.m_components);
		}

		constexpr bool operator>(const ThisType& other) const
		{
			return (m_components > other.m_components);
		}

		template<typename StringClassWithSplitMethod>
		static Version tryParse(const StringClassWithSplitMethod& s, const Version& defaultVersion)
		{
			try {
				return Version(s);
			} catch (const std::runtime_error& er) {
				std::cerr << "Error parsing version:" << er.what();
				return defaultVersion;
			}
		}

	private:
		using ComponentsArray = std::array<T, N>;

		template<typename StringsList>
		static ComponentsArray parseList(const StringsList& versionParts)
		{
			if ((static_cast<std::size_t>(versionParts.size()) > N) ||
			    (static_cast<std::size_t>(versionParts.size()) < Mandatory))
				throw std::runtime_error("Incorrect number of version components");

			// 			bool ok = false;
			ComponentsArray res{{}};
			for (std::size_t i = 0; i < static_cast<std::size_t>(versionParts.size()); ++i) {
				res[i] = boost::lexical_cast<T>(versionParts[static_cast<typename StringsList::size_type>(i)]);
				// 				if (!ok)
				// 					throw std::runtime_error("Can not parse version component");
			}
			return res;
		}

		template<typename StringsList>
		Version(const StringsList& versionParts)
		    : m_components(parseList(versionParts)) // GCC 4.8.4 has problems with the uniform initializer here
		{
		}

		ComponentsArray m_components;
	};

	template<typename T, std::size_t N, std::size_t Mandatory>
	constexpr bool operator!=(const Version<T, N, Mandatory>& left, const Version<T, N, Mandatory>& right)
	{
		return !(left == right);
	}

	template<typename T, std::size_t N, std::size_t Mandatory>
	constexpr bool operator<=(const Version<T, N, Mandatory>& left, const Version<T, N, Mandatory>& right)
	{
		return !(left > right);
	}

	template<typename T, std::size_t N, std::size_t Mandatory>
	constexpr bool operator>=(const Version<T, N, Mandatory>& left, const Version<T, N, Mandatory>& right)
	{
		return !(left < right);
	}
} // namespace twee::utils

#endif // QBITTORRENT_UTILS_VERSION_H
