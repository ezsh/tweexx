/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./config.hxx"
#include "./escaping.hxx"
#include "./impl/json.hxx"
#include "./impl/private_utils.hxx"
#include "./impl/twee2compat.hxx"
#include "./impl/tweelexer.hxx"
#include "./io.hxx"
#include "./story.hxx"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <fmt/ostream.h>
#include <spdlog/spdlog.h>

#include <exception>
#include <format>
#include <sstream>
#include <string_view>
#include <utility>

void twee::Story::loadTwee(const std::filesystem::path& filename, string_view encoding, bool trim, bool twee2Compat)
{
	if (encoding.empty()) {
		throw std::runtime_error("Empty encoding string");
	}
	auto passageCount = this->passages_.size();

	auto source = io::readAllWithEncoding(filename, encoding);
	auto sourceSize = source.size();
	const auto relativeFilename = filename.lexically_proximate(baseDir());

	if (twee2Compat) { source = impl::toV3(source); }

	int pCount = 0;
	lexer::ItemType lastType;
	Passage p;

	auto itemHandler = [this, trim, &pCount, &p, &lastType, &relativeFilename](const lexer::Item& item) {
		switch (item.type) {
			case lexer::ItemType::Error:
				spdlog::error("file {}, line {}: Malformed twee source: {}", fmt::streamed(relativeFilename),
							  item.line, item.val);
				return;
			case lexer::ItemType::EndOfInput:
				// Add the final passage, if any.
				if (pCount > 0) { push_back(std::move(p)); }
				return;

			case lexer::ItemType::Header:
				pCount++;
				if (pCount > 1) { push_back(std::move(p)); }
				break;
			case lexer::ItemType::Name:
				p = Passage(boost::algorithm::trim_copy(tweeUnescape(item.val)));
				p.setSourceFilename(relativeFilename);
				if (p.name().empty()) {
					spdlog::error("file {}, line {}: Malformed twee source; passage with no name.",
								  fmt::streamed(relativeFilename), item.line);
					return;
				}
				break;
			case lexer::ItemType::Tags:
				if (lastType != lexer::ItemType::Name) {
					spdlog::error(
						"file {}, line {}: Malformed twee source; optional tags block must immediately follow the passage name.",
						fmt::streamed(relativeFilename), item.line);
					return;
				}
				{
					std::vector<std::string> tags;
					boost::algorithm::split(tags, item.val.substr(1, item.val.size() - 2), boost::algorithm::is_space(),
						boost::token_compress_on);
					p.setTags(tags);
				}
				break;

			case lexer::ItemType::Metadata:
				if (lastType != lexer::ItemType::Name && lastType != lexer::ItemType::Tags) {
					spdlog::error("file {}, line {}: malformed twee source; optional metadata block must immediately "
								  "follow the passage name or tags block.", fmt::streamed(relativeFilename), item.line);
					return;
				}
					try {
						const impl::json::JsonObject metaJson = impl::json::jsonFromString(item.val);
						Passage::Metadata meta;
						meta.position = impl::json::getValue(metaJson, "position", meta.position);
						meta.size = impl::json::getValue(metaJson, "size", meta.size);
						p.setMetadata(std::move(meta));
					} catch (std::exception& ex) {
						spdlog::warn("file {}, line {}: Malformed twee source; could not decode metadata. Reason: {}",
							fmt::streamed(relativeFilename), item.line, ex.what());
					}
				break;

			case lexer::ItemType::Content:
				if (trim) {
					// Trim whitespace surrounding (leading and trailing) passages.
					p.setText(boost::algorithm::trim_copy(item.val));
				} else {
					// Do not trim whitespace surrounding passages.
					p.setText(item.val);
				}
				break;
		}
		lastType = item.type;
	};

	auto lex = lexer::TweeLexer(source, itemHandler);
	lex.run();

	if (passages_.size() == passageCount) {
		spdlog::info("Loaded 0 passages from file {} of size {} with encoding {}. Twee2 compatibility: {}",
			fmt::streamed(filename), sourceSize, encoding, twee2Compat);
	}
}

void twee::Story::loadTagged(const std::string& tag, const std::filesystem::path& filename, string_view encoding)
{
	push_back(Passage(filename.filename().string(), filename.lexically_proximate(baseDir()), {tag}, io::readAllWithEncoding(filename, encoding)));
}

void twee::Story::loadMedia(const std::string& tag, const std::filesystem::path& filename)
{
	push_back(Passage(
		filename.stem().string(), filename.lexically_proximate(baseDir()), {tag},
		std::format("data:{};base64,{}", impl::mimeTypeFromFilename(filename), io::readAllAsBase64(filename))));
}

void twee::Story::loadFont(const std::filesystem::path& filename)
{
	using namespace std::string_view_literals;

	const auto source = io::readAllAsBase64(filename);
	const auto name = filename.stem().string();
	std::vector<std::string> nameParts;
	boost::algorithm::split(nameParts, name, boost::is_any_of("."));
	const auto family = nameParts[0];
	const auto ext = impl::normalizedFileExt(filename);
	std::string hint;
	if (ext == "ttf"sv) {
		hint = "truetype"sv;
	} else if (ext == "otf"sv) {
		hint = "opentype"sv;
	} else {
		hint = ext;
	}

	std::ostringstream content;
	content << "@font-face {\n\tfont-family: " << family << ";\n\tsrc: url(\"data:" << impl::mimeTypeFromExt(ext)
			<< ";base64," << source << "\") format(" << hint << ");\n}";

	push_back(Passage(name, filename.lexically_proximate(baseDir()), {"stylesheet"}, content.str()));
}
