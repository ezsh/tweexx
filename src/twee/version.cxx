/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#include "./version.hxx"

#include "impl/private_utils.hxx"

std::vector<std::string_view> twee::utils::impl::split(std::string_view s)
{
	return twee::impl::split(s, '.');
}
