/*
    Copyright © 2020 ezsh. All rights reserved.
    Use of this source code is governed by a Simplified BSD License which
    can be found in the LICENSE file.
*/

#ifndef TWEEXX_CONFIG_HXX
#define TWEEXX_CONFIG_HXX

#include <filesystem>
#include <string>
#include <vector>

#include "twee-export.h"

namespace twee {
	class Story;

	struct StoryLoadingOptions {
		std::string encoding = "UTF-8";
		bool trim = true;
		bool twee2Compat = false;
		std::filesystem::path baseDir = std::filesystem::current_path();
	};
} // namespace twee

namespace twee::environment {

	TWEE_API std::vector<std::filesystem::path> storyFormatDirs();

} // namespace twee::environment

#endif
